#include "char_predicates.h"
#include "scanner.h"
#include "source.h"

Scanner::Scanner(Source& source) 
        : source_(source)
        , c0_(-1)
        , curr_token_pos_(0)
{ }

Token Scanner::GetCurrentToken() {
    return tokens_[curr_token_pos_];
}

void Scanner::MoveToNextToken() {
    curr_token_pos_++;
}

void Scanner::Scan() {
    do {
        Advance();
        switch (c0_) {
        case '\n':
            tokens_.push_back(Token(TK_EOL));
            break;
        case '+':
            tokens_.push_back(Token(TK_PLUS));
            break;
        case '-':
            // - --
            Advance();
            if (c0_ == '-') {
                ScanComment();
            } 
            else {
                tokens_.push_back(Token(TK_MINUS));
                PushBack();
            }
            break;
        case '*':
            tokens_.push_back(Token(TK_STAR));
            break;
        case '/':
            tokens_.push_back(Token(TK_SLASH));
            break;
        case '%':
            tokens_.push_back(Token(TK_MOD));
            break;
        case '^':
            tokens_.push_back(Token(TK_UP_ARROW));
            break;
        case '#':
            tokens_.push_back(Token(TK_SHARP));
            break;
        case '=':
            // = ==
            Advance();
            if (c0_ == '=') {
                tokens_.push_back(Token(TK_EQUALS_EQUALS));
            }
            else {
                tokens_.push_back(Token(TK_EQUALS));
                PushBack();
            }
            break;
        case '~':
            // ~=
            Advance();
            if (c0_ == '=') {
                tokens_.push_back(Token(TK_NOT_EQUALS));
            }
            else {
                // should not be here
                PushBack();
            }
            break;
       case '<':
            // < <=
            Advance();
            if (c0_ == '=') {
                tokens_.push_back(Token(TK_LESS_EQUALS));
            }
            else {
                tokens_.push_back(Token(TK_LESS));
                PushBack();
            }
            break;
        case '>':
            // > >=
            Advance();
            if (c0_ == '=') {
                tokens_.push_back(Token(TK_GREATER_EQUALS));
            }
            else {
                tokens_.push_back(Token(TK_GREATER));
                PushBack();
            }
            break;
        case '(':
            tokens_.push_back(Token(TK_LEFT_PAREN));
            break;
        case ')':
            tokens_.push_back(Token(TK_RIGHT_PAREN));
            break;
        case '{':
            tokens_.push_back(Token(TK_LEFT_BRACE));
            break;
        case '}':
            tokens_.push_back(Token(TK_RIGHT_BRACE));
            break;
        case '[':
            tokens_.push_back(Token(TK_LEFT_BRACKET));
            break;
        case ']':
            tokens_.push_back(Token(TK_RIGHT_BRACKET));
            break;
        case ';':
            tokens_.push_back(Token(TK_SEMICOLON));
            break;
        case ':':
            tokens_.push_back(Token(TK_COLON));
            break;
        case ',':
            tokens_.push_back(Token(TK_COMMA));
            break;
        case '.':
            // . .. ... number
            Advance();
            if (c0_ == '.') {
                Advance();
                if (c0_ == '.') {
                    tokens_.push_back(Token(TK_DOT_DOT_DOT));
                }
                else {
                    PushBack();
                    tokens_.push_back(Token(TK_DOT_DOT));
                }
            }
            else if (IsDecimalDigit(c0_)) {
                PushBack();
                ScanFloatingNumber();
            }
            else {
                PushBack();
                tokens_.push_back(Token(TK_DOT));
            }
            break;
        case '0':
            Advance();
            // hexadecimal number
            if (AsciiAlphaToLower(c0_) == 'x') {
                ScanHexNumber();
            }
            else {
                PushBack();
                ScanNumber();
            }
            break;
        case '\"':
            ScanString();
            break;
        default:
            // number
            if (IsDecimalDigit(c0_)) {
                ScanNumber();
            }
            // name or reserved word
            else if (IsAsciiAlpha(c0_)) {
                ScanWord();
            }
            break;
        }
    }
    while (c0_ > 0);
    tokens_.push_back(Token(TK_EOS));
}

void Scanner::ScanComment() {
    while (1) {
        Advance();
        if (c0_ == '\n' || c0_ <= 0) {
            PushBack();
            break;
        }
        buffer_ << c0_;
    }       
    tokens_.push_back(Token(TK_COMMENT, buffer_.str()));
    ResetBuffer();
}

void Scanner::ScanFloatingNumber() {
    buffer_ << c0_;
    bool has_exponent = false;
    while (1) {
        Advance();
        if (AsciiAlphaToLower(c0_) == 'e') {
            has_exponent = true;
            ScanFloatingNumberExponent();
            break;
        }
        if (!IsDecimalDigit(c0_)) {
            PushBack();
            break;
        }
        buffer_ << c0_;
    }
    if (!has_exponent) {
        tokens_.push_back(Token(TK_NUMBER, buffer_.str()));
        ResetBuffer();
    }
}

void Scanner::ScanFloatingNumberExponent() {
    buffer_ << c0_;
    Advance();
    if (c0_ == '+' || c0_ == '-') {
        buffer_ << c0_;
    }
    else {
        PushBack();
    }
    ScanFloatingNumberExponentPrefixed();
}

void Scanner::ScanFloatingNumberExponentPrefixed() {
    while (1) {
        Advance();
        if (!IsDecimalDigit(c0_)) {
            PushBack();
            break;
        }
        buffer_ << c0_;
    }       
    tokens_.push_back(Token(TK_NUMBER, buffer_.str())); 
    ResetBuffer();
}

void Scanner::ScanNumber() {
    buffer_ << c0_;
    bool is_decimal_number = true;
    while (1) {
        Advance();
        if (IsDecimalDigit(c0_)) {
            buffer_ << c0_;
        }
        else if (c0_ == '.') {
            ScanFloatingNumber();
            is_decimal_number = false;
            break;
        }
        else if (AsciiAlphaToLower(c0_) == 'e') {
            ScanFloatingNumberExponent();
            is_decimal_number = false;
            break;
        }
        else {
            PushBack();
            break;
        }
    }

    if (is_decimal_number) {
        tokens_.push_back(Token(TK_NUMBER, buffer_.str()));
        ResetBuffer();
    }
}

void Scanner::ScanHexNumber() {
    buffer_ << "0" << c0_;
    while (1) {
        Advance();
        if (!IsHexDigit(c0_)) {
            PushBack();
            break;
        }
        buffer_ << c0_;
    }       
    tokens_.push_back(Token(TK_NUMBER, buffer_.str()));
    ResetBuffer();
}

void Scanner::ScanString() {
    buffer_ << c0_;
    bool is_enclosed = false;
    while (!is_enclosed) {
        Advance();
        buffer_ << c0_;
        switch (c0_) {
        case '\\':
            Advance();
            // escaped characters
            if (c0_ == '\"' || c0_ == '\'' || c0_ == '\\' || c0_ == 'n') {
                buffer_ << c0_;
            }
            else {
                PushBack();
            }
            break;
        case '\"':
            is_enclosed = true;
            break;
        default:
            break;
        }
    }
    tokens_.push_back(Token(TK_STRING, buffer_.str()));
    ResetBuffer();
}

void Scanner::ScanWord() {
    buffer_ << c0_;
    while (1) {
        Advance();
        if (IsAsciiAlpha(c0_) || IsDecimalDigit(c0_) || c0_ == '_') {
            buffer_ << c0_;
        }
        else {
            PushBack();
            break;
        }
    }
    tokens_.push_back(Token::CreateWordToken(buffer_.str()));
    ResetBuffer();
}

void Scanner::Advance() { 
    source_.Advance();
    c0_ = source_.GetCurrentChar();
}

void Scanner::PushBack() {
    source_.PushBack();
    c0_ = source_.GetCurrentChar();
}

void Scanner::ResetBuffer() {
    buffer_.str("");
}