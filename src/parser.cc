#include <assert.h>
#include <iostream>
#include <sstream>
#include <stack>
#include <vector>

#include "ast.h"
#include "parser.h"
#include "scanner.h"
#include "source.h"

Parser::Parser(Source& source) 
        : source_(source)
        , scanner_(0)
{ }

Parser::~Parser() {
    delete ast_;
    ast_ = 0;
}

void Parser::Parse() {
    scanner_ = new Scanner(source_);
    scanner_->Scan();
    ast_ = ParseBlock();
    delete scanner_;
    scanner_ = 0;
}

void Parser::PrintAbstractSyntaxTree() const {
    std::cout << ast_->ToString() << std::endl;
}

Identifier* Parser::ParseIdentifier() {
    assert(GetCurrentToken().type() == TK_IDENTIFIER);
    Identifier* id = new Identifier(GetCurrentToken().text());
    ConsumeToken();
    return id;
}

Block* Parser::ParseBlock() {
    // Block -> { Statement <EOL> } [ LastStatement <EOL> ]
    std::vector<Statement*> statements;
    while (1) {
        TokenType type = GetCurrentToken().type();
        if (type == TK_EOS || type == TK_END || type == TK_UNTIL ||
                type == TK_ELSEIF || type == TK_ELSE) {
            break;
        }
        else if (type == TK_RETURN || type == TK_BREAK) {
            statements.push_back(ParseLastStatement());
            ConsumeToken(); // consume <EOL>
        }
        else {
            statements.push_back(ParseStatement());
            ConsumeToken(); // consume <EOL>
        }
    }
    return new Block(statements);
}

Statement* Parser::ParseStatement() {
    // Statement -> SimpleStatement | ControlFlow | FunctionDefinition |
    //              LocalVariableDeclaration
    TokenType type = GetCurrentToken().type();
    if (type == TK_WHILE || type == TK_REPEAT 
            || type == TK_IF || type == TK_FOR) {
        return ParseControlFlow();
    }
    if (type == TK_FUNCTION) {
        return ParseFunctionDefinition();
    }
    if (type == TK_LOCAL) {
        return ParseLocalVariableDeclaration();
    }
    return ParseSimpleStatement();
}

SimpleStatement* Parser::ParseSimpleStatement() {
    // SimpleStatement -> EmptyStatement | AssignmentStatement | 
    //                    FunctionCall | BlockStatement
    TokenType type = GetCurrentToken().type();
    if (type == TK_EOL) {
        return ParseEmptyStatement();
    } 
    else if (type == TK_DO) {
        return ParseBlockStatement();
    }
    Variable* variable = ParseVariable();
    type = GetCurrentToken().type();
    if (type == TK_EQUALS) {
        return ParseAssignmentStatement(variable);
    } 
    if (type == TK_LEFT_PAREN) {
        return ParseFunctionCall(variable);
    }
    assert(false);
    return new Nil();
}

EmptyStatement* Parser::ParseEmptyStatement() {
    // EmptyStatement -> 
    return new EmptyStatement();
}

ControlFlow* Parser::ParseControlFlow() {
    // ControlFlow -> WhileStatement | RepeatStatement | IfStatement |
    //                ForStatement | ForInStatement
    TokenType type = GetCurrentToken().type();
    if (type == TK_WHILE) {
        return ParseWhileStatement();
    }
    if (type == TK_REPEAT) {
        return ParseRepeatStatement();
    }
    if (type == TK_IF) {
        return ParseIfStatement();
    }
    assert(type == TK_FOR);
    ConsumeToken();
    Variable* each = ParseVariable();
    type = GetCurrentToken().type();
    if (type == TK_EQUALS) {
        return ParseForStatement(each);
    }
    else {
        assert(type == TK_IN);
        return ParseForInStatement(each);
    }
}

FunctionDefinition* Parser::ParseFunctionDefinition() {
    // FunctionDefinition -> 'function' <IDENTIFIER> 
    //                       '(' [ <IDENTIFIER> {',' <IDENTIFIER>} ] ')' 
    //                       Block 'end'
    assert(GetCurrentToken().type() == TK_FUNCTION);
    ConsumeToken();
    Variable* name = ParseVariable();
    std::vector<Variable*> parameters;
    Token token = GetCurrentToken();
    assert(token.type() == TK_LEFT_PAREN);
    ConsumeToken(); // consume '(';
    token = GetCurrentToken();
    bool is_first_parameter = true;
    while (true) {
        if (token.type() == TK_RIGHT_PAREN) {
            ConsumeToken(); // consume ')';
            break;
        }
        if (is_first_parameter) {
            is_first_parameter = false;
        } 
        else {
            assert(token.type() == TK_COMMA);
            ConsumeToken(); // consume ','
        }
        parameters.push_back(ParseVariable());
        token = GetCurrentToken(); 
    }
    Block* body = ParseBlock();
    assert(GetCurrentToken().type() == TK_END);
    ConsumeToken();
    return new FunctionDefinition(name, parameters, body);
}

LocalVariableDeclaration* Parser::ParseLocalVariableDeclaration() {
    // LocalVariableDeclaration -> 'local' <IDENTIFIER> [ '=' Expression ]
    assert(GetCurrentToken().type() == TK_LOCAL);
    ConsumeToken();
    Variable* name = ParseVariable();
    Expression* value = 0;
    if (GetCurrentToken().type() == TK_EQUALS) {
        ConsumeToken();
        value = ParseExpression();
    }
    else {
        value = new Nil();
    }
    return new LocalVariableDeclaration(name, value);
}

LastStatement* Parser::ParseLastStatement() {
    // LastStatement -> ReturnStatement | BreakStatement
    TokenType type = GetCurrentToken().type();
    assert(type == TK_RETURN || type == TK_BREAK);
    if (type == TK_RETURN) {
        return ParseReturnStatement();
    }
    else {
        return ParseBreakStatement();
    }
}

AssignmentStatement* Parser::ParseAssignmentStatement(
        Variable* parsed_target) 
{
    // AssignmentStatement -> Variable AssignmentOperator Expression
    assert(GetCurrentToken().type() == TK_EQUALS);
    ConsumeToken(); // consume AssignmentOperator;
    Expression* value = ParseExpression();
    return new AssignmentStatement(parsed_target, value);
}

FunctionCall* Parser::ParseFunctionCall(Variable* parsed_callee) {
    // FunctionCall -> Variable '(' [ Expression { , Expression } ] ')'
    Token token = GetCurrentToken();
    assert(token.type() == TK_LEFT_PAREN);
    ConsumeToken(); // consume '(';
    std::vector<Expression*> args;
    token = GetCurrentToken();
    bool is_first_arg = true;
    while (true) {
        if (token.type() == TK_RIGHT_PAREN) {
            ConsumeToken(); // consume ')';
            break;
        }
        if (is_first_arg) {
            is_first_arg = false;
        } 
        else {
            assert(token.type() == TK_COMMA);
            ConsumeToken(); // consume ','
        }
        args.push_back(ParseExpression());
        token = GetCurrentToken(); 
    }
    return new FunctionCall(parsed_callee, args);
}

BlockStatement* Parser::ParseBlockStatement() {
    // BlockStatement -> 'do' Block 'end'
    assert(GetCurrentToken().type() == TK_DO);
    ConsumeToken();
    Block* body = ParseBlock();
    assert(GetCurrentToken().type() == TK_END);
    ConsumeToken();
    return new BlockStatement(body);
}

Expression* Parser::ParseExpression() {
    // Expression -> UnaryExpression | ParenExpression | 
    //               SimpleExpression | BinaryExpression
    return ParseExpressionImpl(ParsePrimaryExpression(), 0);
}

Expression* Parser::ParseExpressionImpl(Expression* left, int precedence) {
    // Operator-precedence parser
    Token op = GetCurrentToken();
    while (Token::IsBinaryOp(op) && Token::GetPrecedence(op) >= precedence) {
        ConsumeToken(); // consume BinaryOperator
        Expression* right = ParsePrimaryExpression();
        assert(right);
        Token next = GetCurrentToken();
        while (Token::IsBinaryOp(next) && 
                ((Token::GetPrecedence(next) > Token::GetPrecedence(op)) || 
                (Token::IsRightAssociative(next) && 
                (Token::GetPrecedence(next) == Token::GetPrecedence(op)))))
        {
            right = ParseExpressionImpl(right, Token::GetPrecedence(next));
            assert(right);
            next = GetCurrentToken();
        }
        left = new BinaryExpression(left, op, right);
        assert(left);
        op = GetCurrentToken();
    }
    return left;
}

Expression* Parser::ParsePrimaryExpression() {
    // PrimaryExpression -> UnaryExpression | ParenExpression | 
    //                      SimpleExpression
    if (Token::IsUnaryOp(GetCurrentToken())) {
        return ParseUnaryExpression();
    } 
    if (GetCurrentToken().type() == TK_LEFT_PAREN) {
        return ParseParenExpression();
    }
    return ParseSimpleExpression();
}

SimpleExpression* Parser::ParseSimpleExpression() {
    // SimpleExpression -> <NUMBER> | <STRING> | 'nil' | 'true' | 'false' | 
    //                     EmptyTable | Variable | FunctionCall
    TokenType type = GetCurrentToken().type();
    if (type == TK_NUMBER) {
        return ParseNumber();
    }
    if (type == TK_STRING) {
        return ParseString();
    }
    if (type == TK_NIL) {
        return ParseNil();
    }
    if (type == TK_TRUE || type == TK_FALSE) {
        return ParseBoolean(type);
    }    
    if (type == TK_LEFT_BRACE) {
        return ParseEmptyTable();
    }
    Variable* variable = ParseVariable();
    assert(variable);
    if (GetCurrentToken().type() == TK_LEFT_PAREN) {
        return ParseFunctionCall(variable);
    }
    return variable;
}

UnaryExpression* Parser::ParseUnaryExpression() {
    // UnaryExpression -> UnaryOperator Expression    
    Token op = GetCurrentToken();
    assert(Token::IsUnaryOp(op));
    ConsumeToken(); // consume UnaryOperator
    Expression* expression = ParseExpression();
    assert(expression);
    return new UnaryExpression(op, expression);
}

ParenExpression* Parser::ParseParenExpression() {
    // ParenExpression -> '(' Expression ')'
    assert(GetCurrentToken().type() == TK_LEFT_PAREN);
    ConsumeToken(); // consume '('
    Expression* expression = ParseExpression();
    assert(GetCurrentToken().type() == TK_RIGHT_PAREN);
    ConsumeToken(); // consume ')'
    return new ParenExpression(expression);
}

Number* Parser::ParseNumber() {
    assert(GetCurrentToken().type() == TK_NUMBER);
    double value;
    std::stringstream number(GetCurrentToken().text());
    number >> value;
    ConsumeToken();
    return new Number(value);
}

String* Parser::ParseString() {
    assert(GetCurrentToken().type() == TK_STRING);
    std::string text = GetCurrentToken().text();
    std::stringstream value;
    // Skip double quotes and escape characters
    for (int i = 1; i < text.size() - 1; i++) {
        if (text[i] == '\\') {
            i++;
        }
        value << text[i];
    }
    ConsumeToken();
    return new String(value.str());
}

Nil* Parser::ParseNil() {
    assert(GetCurrentToken().type() == TK_NIL);
    ConsumeToken();
    return new Nil();
}

Boolean* Parser::ParseBoolean(TokenType parsed_type) {
    assert(parsed_type == TK_TRUE || parsed_type == TK_FALSE);
    ConsumeToken();
    return new Boolean(parsed_type == TK_TRUE);
}

EmptyTable* Parser::ParseEmptyTable() {
    // EmptyTable -> '{' '}'
    assert(GetCurrentToken().type() == TK_LEFT_BRACE);
    ConsumeToken();
    assert(GetCurrentToken().type() == TK_RIGHT_BRACE);
    ConsumeToken();
    return new EmptyTable();
}

Variable* Parser::ParseVariable() {
    // Variable -> <IDENTIFIER> { '.' <IDENTIFIER> | '[' Expression ']' }
    Identifier* name = ParseIdentifier();
    std::vector<Expression*> index;
    Token token = GetCurrentToken();
    while (true) {
        if (token.type() == TK_DOT) {
            ConsumeToken(); // consume '.'
            token = GetCurrentToken(); 
            index.push_back(new String(token.text()));
            ConsumeToken();
        }
        else if (token.type() == TK_LEFT_BRACKET) {
            ConsumeToken(); // consume '['
            index.push_back(ParseExpression());
            assert(GetCurrentToken().type() == TK_RIGHT_BRACKET);
            ConsumeToken(); // consume ']'
        }
        else {
            break;
        }
        token = GetCurrentToken(); 
    }
    return new Variable(name, index);
}

WhileStatement* Parser::ParseWhileStatement() {
    // WhileStatement -> 'while' Expression 'do' Block 'end'
    assert(GetCurrentToken().type() == TK_WHILE);
    ConsumeToken();
    Expression* condition = ParseExpression();
    assert(GetCurrentToken().type() == TK_DO);
    ConsumeToken();
    Block* body = ParseBlock();
    assert(GetCurrentToken().type() == TK_END);
    ConsumeToken();
    return new WhileStatement(condition, body);
}

RepeatStatement* Parser::ParseRepeatStatement() {
    // RepeatStatement -> 'repeat' Block 'until' Expression
    assert(GetCurrentToken().type() == TK_REPEAT);
    ConsumeToken();
    Block* body = ParseBlock();
    assert(GetCurrentToken().type() == TK_UNTIL);
    ConsumeToken();
    Expression* condition = ParseExpression();
    return new RepeatStatement(body, condition);
}

IfStatement* Parser::ParseIfStatement() {
    // IfStatement -> 'if' Expression 'then' Block 
    //                { 'elseif' Expression 'then' Block } 
    //                [ 'else' Block ] 'end'
    std::stack<Expression*> condition_stack;
    std::stack<Block*> then_stack;
    ParseIfStatementConditionBodyStack(&condition_stack, &then_stack);
    Block* else_body = ParseIfStatementElseBody();
    return CreateNestedIfStatement(&condition_stack, &then_stack, else_body);
}

void Parser::ParseIfStatementConditionBodyStack(
        std::stack<Expression*>* condition_stack,
        std::stack<Block*>* then_stack) 
{
    assert(GetCurrentToken().type() == TK_IF);
    ConsumeToken(); // consume 'if'
    condition_stack->push(ParseExpression());
    assert(GetCurrentToken().type() == TK_THEN);
    ConsumeToken(); // consume 'then'
    then_stack->push(ParseBlock());
    while (GetCurrentToken().type() == TK_ELSEIF) {
        ConsumeToken(); // consume 'elseif'
        condition_stack->push(ParseExpression());
        assert(GetCurrentToken().type() == TK_THEN);
        ConsumeToken(); // consume 'then'
        then_stack->push(ParseBlock());
    }
}

Block* Parser::ParseIfStatementElseBody() {
    if (GetCurrentToken().type() == TK_ELSE) {
        ConsumeToken(); // consume 'else'
        return ParseBlock();
    }
    return CreateBlock(new EmptyStatement());
}

IfStatement* Parser::CreateNestedIfStatement(
        std::stack<Expression*>* condition_stack,
        std::stack<Block*>* then_stack,
        Block* else_body) 
{
    IfStatement* rv = new IfStatement(condition_stack->top(), 
                                      then_stack->top(), 
                                      else_body);
    condition_stack->pop();
    then_stack->pop();
    while (!condition_stack->empty()) {
        rv = new IfStatement(condition_stack->top(), 
                             then_stack->top(), 
                             CreateBlock(rv));
        condition_stack->pop();
        then_stack->pop();
    }
    assert(GetCurrentToken().type() == TK_END);
    ConsumeToken();
    return rv;
}

ForStatement* Parser::ParseForStatement(Variable* parsed_each) {
    // ForStatement -> 'for' <IDENTIFIER> '=' Expression ',' 
    //                 Expression [',' Expression] 'do' Block 'end'
    assert(GetCurrentToken().type() == TK_EQUALS);
    ConsumeToken();
    Expression* initial_value = ParseExpression();
    assert(GetCurrentToken().type() == TK_COMMA);
    ConsumeToken();
    Expression* limit_value = ParseExpression();
    Expression* step_value = 0;
    if (GetCurrentToken().type() == TK_COMMA) {
        ConsumeToken();
        step_value = ParseExpression();
    }
    else {
        step_value = new Number(1); // default step
    }
    assert(GetCurrentToken().type() == TK_DO);
    ConsumeToken();
    Block* body = ParseBlock();
    assert(GetCurrentToken().type() == TK_END);
    ConsumeToken();
    return new ForStatement(parsed_each, 
                            initial_value, 
                            limit_value, 
                            step_value, 
                            body);
}

ForInStatement* Parser::ParseForInStatement(Variable* parsed_each) {
    // ForInStatement -> 'for' <IDENTIFIER> 'in' Iterator 'do' Block 'end'
    // Iterator -> 'ipairs' | 'pairs' '(' Variable ')'
    assert(GetCurrentToken().type() == TK_IN);
    ConsumeToken();
    Token token = GetCurrentToken();
    assert(token.type() == TK_IDENTIFIER);
    assert(token.text() == "ipairs" || token.text() == "pairs");
    ConsumeToken();
    assert(GetCurrentToken().type() == TK_LEFT_PAREN);
    ConsumeToken();
    Variable* iterator = ParseVariable();
    assert(GetCurrentToken().type() == TK_RIGHT_PAREN);
    ConsumeToken();
    assert(GetCurrentToken().type() == TK_DO);
    ConsumeToken();
    Block* body = ParseBlock();
    assert(GetCurrentToken().type() == TK_END);
    ConsumeToken();
    return new ForInStatement(parsed_each, iterator, body);
}

ReturnStatement* Parser::ParseReturnStatement() {
    // ReturnStatement -> 'return' [Expression]
    assert(GetCurrentToken().type() == TK_RETURN);
    ConsumeToken();
    if (GetCurrentToken().type() == TK_EOL) {
        return new ReturnStatement();
    }
    Expression* value = ParseExpression();
    return new ReturnStatement(value);
}

BreakStatement* Parser::ParseBreakStatement() {
    // BreakStatement -> 'break'
    assert(GetCurrentToken().type() == TK_BREAK);
    ConsumeToken();
    return new BreakStatement();
}

Token Parser::GetCurrentToken() {
    while (scanner_->GetCurrentToken().type() == TK_COMMENT) {
        scanner_->MoveToNextToken();
    }
    return scanner_->GetCurrentToken();
}

void Parser::ConsumeToken() {
    scanner_->MoveToNextToken();
}

Block* Parser::CreateBlock(Statement* statement) {
    std::vector<Statement*> statements;
    statements.push_back(statement);
    return new Block(statements);
}