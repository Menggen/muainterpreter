#ifndef MUA_INTERPRETER_SOURCE_H_
#define MUA_INTERPRETER_SOURCE_H_

#include <vector>

class Source 
{
public:
    explicit Source(std::vector<char>& source);
    explicit Source(std::string source);
    char GetCurrentChar() const;

    void Advance();
    void PushBack();

private:
    std::vector<char> source_;
    int pos_;
    char c0_;
};

#endif  // MUA_INTERPRETER_SOURCE_H_