#include <assert.h>

#include "runtime_stack.h"

RuntimeStack::RuntimeStack()
        : break_command_stack_(0)
{ }

RuntimeStack::~RuntimeStack() {
    assert(!HasBreakCommand());
    assert(!HasReturnValue());
}

void RuntimeStack::PushBreakCommand() {
    break_command_stack_++;
}

bool RuntimeStack::HasBreakCommand() const {
    return break_command_stack_ > 0;
}

void RuntimeStack::PopBreakCommand() {
    assert(HasBreakCommand());
    break_command_stack_--;
}

void RuntimeStack::PushReturnValue(SimpleExpression* value) {
    return_value_stack_.push(value);
}
bool RuntimeStack::HasReturnValue() const {
    return !return_value_stack_.empty();
}

SimpleExpression* RuntimeStack::PopReturnValue() {
    assert(HasReturnValue());
    SimpleExpression* rv = return_value_stack_.top();
    return_value_stack_.pop();
    return rv;
}
