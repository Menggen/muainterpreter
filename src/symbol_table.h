#ifndef MUA_INTERPRETER_SYMBOL_TABLE_H_
#define MUA_INTERPRETER_SYMBOL_TABLE_H_

#include <map>
#include <string>
#include <vector>

class Node;
class Variable;

class Symbol
{
public:
    explicit Symbol(Variable* variable, Node* value);

    void SetScope(int scope);

    Variable* variable() const { return variable_; }
    Node* value() const { return value_; }
    int scope() const { return scope_; }

private:
    Variable* variable_;
    Node* value_;
    int scope_;
};

class SymbolTable 
{
public:
    SymbolTable();
    ~SymbolTable() { }

    void EnterScope();
    void LeaveScope();
    int GetScopeLevel() const { return symbol_stack_.size(); };
    void Insert(Symbol* symbol);
    void InsertLocal(Symbol* symbol);
    Symbol* Lookup(Variable* variable);
    int GetTableSymbolCount(Variable* variable);
    
private:
    std::string GetKey(Symbol* symbol);
    std::string GetKey(Variable* variable);
    
    typedef std::map<std::string, Symbol*> SymbolTableHash;
    std::vector<SymbolTableHash> symbol_stack_; 
};

#endif  // MUA_INTERPRETER_SYMBOL_TABLE_H_