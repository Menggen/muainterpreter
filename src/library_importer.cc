#include <vector>
#include <math.h>

#include "library_importer.h"
#include "ast.h"
#include "symbol_table.h"

LibraryImporter::LibraryImporter(SymbolTable* symbol_table)
        : symbol_table_(symbol_table)
{ }

void LibraryImporter::Import() {
    ImportLibraryPrintFunction();
    /*
    ImportLibraryTonumberFunction();
    ImportLibraryTostringFunction();
    ImportLibraryStringRepFunction();
    ImportLibraryStringSubFunction();
    ImportLibraryTableConcatFunction();
    ImportLibraryTableSortFunction();
    ImportLibraryMathAbsFunction();
    ImportLibraryMathFloorFunction();
    ImportLibraryMathCeilFunction();
    ImportLibraryMathSqrtFunction();
    ImportLibraryMathExpFunction();
    ImportLibraryMathLogFunction();
    ImportLibraryMathLog10Function();
    ImportLibraryMathPiConstant();
    ImportLibraryMathRadFunction();
    ImportLibraryMathDegFunction();
    ImportLibraryMathAcosFunction();
    ImportLibraryMathAsinFunction();
    ImportLibraryMathAtanFunction();
    ImportLibraryMathAtan2Function();
    ImportLibraryMathCosFunction();
    ImportLibraryMathSinFunction();
    ImportLibraryMathTanFunction();
    ImportLibraryMathMinFunction();
    ImportLibraryMathMaxFunction();
    */
}


void LibraryImporter::ImportLibraryPrintFunction() {
    PrintFunction* function = new PrintFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryTonumberFunction() {
    TonumberFunction* function = new TonumberFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryTostringFunction() {
    TostringFunction* function = new TostringFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryStringRepFunction() {
    StringRepFunction* function = new StringRepFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryStringSubFunction() {
    StringSubFunction* function = new StringSubFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryTableConcatFunction() {
    TableConcatFunction* function = new TableConcatFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryTableSortFunction() {
    TableSortFunction* function = new TableSortFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathAbsFunction() {
    MathAbsFunction* function = new MathAbsFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathFloorFunction() {
    MathFloorFunction* function = new MathFloorFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathCeilFunction() {
    MathCeilFunction* function = new MathCeilFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathSqrtFunction() {
    MathSqrtFunction* function = new MathSqrtFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathExpFunction() {
    MathExpFunction* function = new MathExpFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathLogFunction() {
    MathLogFunction* function = new MathLogFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathLog10Function() {
    MathLog10Function* function = new MathLog10Function();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathPiConstant() {
    std::vector<Expression*> index;
    index.push_back(new String("pi"));
    Variable* PI = new Variable(new Identifier("math"), index);
    symbol_table_->Insert(new Symbol(PI, new Number(acos(-1.0))));    
}

void LibraryImporter::ImportLibraryMathRadFunction() {
    MathRadFunction* function = new MathRadFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathDegFunction() {
    MathDegFunction* function = new MathDegFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathAcosFunction() {
    MathAcosFunction* function = new MathAcosFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathAsinFunction() {
    MathAsinFunction* function = new MathAsinFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathAtanFunction() {
    MathAtanFunction* function = new MathAtanFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathAtan2Function() {
    MathAtan2Function* function = new MathAtan2Function();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathCosFunction() {
    MathCosFunction* function = new MathCosFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathSinFunction() {
    MathSinFunction* function = new MathSinFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathTanFunction() {
    MathTanFunction* function = new MathTanFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathMinFunction() {
    MathMinFunction* function = new MathMinFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}

void LibraryImporter::ImportLibraryMathMaxFunction() {
    MathMaxFunction* function = new MathMaxFunction();
    symbol_table_->Insert(new Symbol(function->name(), function));
}