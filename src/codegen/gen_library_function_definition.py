import os
import sys

import library_function

ROOT_DIR = '../ast/library/'

BLOCK_HEADER_TEMPLATE = """#ifndef MUA_INTERPRETER_AST_LIBRARY_{guard}_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_{guard}_BLOCK_H_

#include "{level}block.h"

class {class_name}Block : public Block 
{{
public:
    virtual {class_name}Block* As{class_name}Block() {{ return this; }}
}};

#endif  // MUA_INTERPRETER_AST_LIBRARY_{guard}_BLOCK_H_
"""

FUNCTION_HEADER_TEMPLATE = """#ifndef MUA_INTERPRETER_AST_LIBRARY_{guard}_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_{guard}_FUNCTION_H_

#include "{level}function_definition.h"

class {class_name}Function : public FunctionDefinition 
{{
public:
    explicit {class_name}Function();
}};

#endif  // MUA_INTERPRETER_AST_LIBRARY_{guard}_FUNCTION_H_
"""

FUNCTION_IMPL_TEMPLATE = """#include "{class_file}_function.h"
#include "{class_file}_block.h"
#include "{level}identifier.h"
#include "{level}variable.h"

{class_name}Function::{class_name}Function() {{
{name_init}
{parameters_init}    body_ = new {class_name}Block();
}}
"""

PARAMETER_TEMPLATE = """    parameters_.push_back(
            new Variable(new Identifier("{parameter}"), std::vector<Expression*>()));\n"""

NAME_TEMPLATE = """    std::vector<Expression*> index;
{index_init}    name_ = new Variable(new Identifier("{name}"), index);"""
            
INDEX_TEMPLATE = """    index.push_back(
            new Variable(new Identifier("{index}"), std::vector<Expression*>()));\n"""
            
def generate_block_header(function):
    filepath = get_filepath(function[0], '_block.h')
    content = get_header_content(function[0], BLOCK_HEADER_TEMPLATE)
    write_to_file(filepath, content)

def generate_function_header(function):
    filepath = get_filepath(function[0], '_function.h')
    content = get_header_content(function[0], FUNCTION_HEADER_TEMPLATE)
    write_to_file(filepath, content)

def generate_function_impl(function):
    filepath = get_filepath(function[0], '_function.cc')
    content = get_impl_content(function)
    write_to_file(filepath, content)    
    
def get_header_content(function, template):
    level = '../' * len(function.split('/'))
    guard = function.upper().replace('/', '_')
    class_name = ''.join([ 
        s[:1].upper() + s[1:] for s in function.lower().split('/') 
    ])
    return template.format(
        guard=guard, 
        class_name=class_name, 
        level=level)

def get_impl_content(function):
    splitted = function[0].lower().split('/')

    class_file = splitted[-1]
    class_name = ''.join([ 
        s[:1].upper() + s[1:] for s in splitted 
    ])
    index_init = ''.join([
        INDEX_TEMPLATE.format(index=index) for index in splitted[1:]
    ])
    name_init = NAME_TEMPLATE.format(
        index_init=index_init,
        name=splitted[0])
    parameters_init = ''.join([ 
        PARAMETER_TEMPLATE.format(parameter=parameter) for parameter in function[1]
    ])
    level = '../' * len(function[0].split('/'))
    
    return FUNCTION_IMPL_TEMPLATE.format(
        class_file=class_file,
        class_name=class_name, 
        name_init=name_init, 
        parameters_init=parameters_init,
        level=level)    
    
def get_filepath(function, tail):
    return ROOT_DIR + function + tail
    
def write_to_file(filepath, content):
    dir = os.path.dirname(filepath)
    if not os.path.exists(dir):
        os.makedirs(dir)
    file = open(filepath, 'w')
    file.write(content)
    file.close()

def main():
    for function in library_function.LIBRARY_FUNCTIONS:
        generate_block_header(function)
        generate_function_header(function)
        generate_function_impl(function)
        
if __name__ == '__main__':
    sys.exit(main())
    