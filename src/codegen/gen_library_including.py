import os
import sys

import library_function

ROOT_DIR = '../ast/'

TEMPLATE = """#ifndef MUA_INTERPRETER_AST_LIBRARY_H_
#define MUA_INTERPRETER_AST_LIBRARY_H_

{include_header_list}
#endif  // MUA_INTERPRETER_AST_LIBRARY_H_
"""

INCLUDE_HEADER_TEMPLATE = """#include "ast/library/{function}_function.h"\n"""

def generate():
    filepath = get_filepath()
    content = get_content()
    write_to_file(filepath, content)

def get_content():
    list = ''.join([ 
        INCLUDE_HEADER_TEMPLATE.format(function = function[0]) for 
        function in library_function.LIBRARY_FUNCTIONS])
    return TEMPLATE.format(include_header_list=list)
    
def get_filepath():
    return ROOT_DIR + 'library.h'
    
def write_to_file(filepath, content):
    dir = os.path.dirname(filepath)
    if not os.path.exists(dir):
        os.makedirs(dir)
    file = open(filepath, 'w')
    file.write(content)
    file.close()

def main():
    generate()
    
if __name__ == '__main__':
    sys.exit(main())
    