#ifndef MUA_INTERPRETER_SCANNER_H_
#define MUA_INTERPRETER_SCANNER_H_

#include <sstream>
#include <vector>

#include "token.h"

class Source;

class Scanner 
{
public:
    explicit Scanner(Source& source);
   
    Token GetCurrentToken();
    void MoveToNextToken();
    void Scan();

private:
    void ScanComment();
    void ScanFloatingNumber();
    void ScanFloatingNumberExponent();
    void ScanFloatingNumberExponentPrefixed();
    void ScanNumber();
    void ScanHexNumber();
    void ScanString();
    void ScanWord();

    void Advance();
    void PushBack();
    void ResetBuffer();

    Source& source_;
    char c0_;
    std::stringstream buffer_;

    std::vector<Token> tokens_;
    int curr_token_pos_;
};

#endif  // MUA_INTERPRETER_SCANNER_H_