#ifndef MUA_INTERPRETER_RUNTIME_STACK_H_
#define MUA_INTERPRETER_RUNTIME_STACK_H_

#include <stack>

class SimpleExpression;

class RuntimeStack
{
public:
    RuntimeStack();
    ~RuntimeStack();

    void PushBreakCommand();
    bool HasBreakCommand() const;
    void PopBreakCommand();

    void PushReturnValue(SimpleExpression* value);
    bool HasReturnValue() const;
    SimpleExpression* PopReturnValue();

private:
    int break_command_stack_;
    std::stack<SimpleExpression*> return_value_stack_;
};

#endif  // MUA_INTERPRETER_RUNTIME_STACK_H_