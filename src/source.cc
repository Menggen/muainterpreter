#include "source.h"

Source::Source(std::vector<char>& source) 
        : source_(source)
        , pos_(-1)
        , c0_(-1)
{ }

Source::Source(std::string source)
        : source_(std::vector<char>(source.begin(), source.end()))
        , pos_(-1)
        , c0_(-1)
{ }

char Source::GetCurrentChar() const {
    return c0_;
}

void Source::Advance() {
    pos_++;
    if (static_cast<unsigned int>(pos_) < source_.size()) {
        c0_ = source_[pos_];
    }
    else {
        c0_ = -1;
    }
}

void Source::PushBack() {
    pos_--;
    if (pos_ >= 0) {    
        c0_ = source_[pos_];
    }
    else {
        c0_ = -1;
    }
}