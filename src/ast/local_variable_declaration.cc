#include <sstream>

#include "expression.h"
#include "local_variable_declaration.h"
#include "variable.h"

LocalVariableDeclaration::LocalVariableDeclaration(Variable* name, 
                                                   Expression* value)
        : name_(name)
        , value_(value)
{ }

LocalVariableDeclaration::~LocalVariableDeclaration() {
    if (name_) {
        delete name_;
        name_ = 0;
    }
    if (value_) {
        delete value_;
        value_ = 0;
    }
}

std::string LocalVariableDeclaration::ToString() const {
    std::stringstream rv;
    rv << "local " << name_->ToString();
    if (HasValue()) {
        rv << " = " << value_->ToString();
    }
    return rv.str();
}
