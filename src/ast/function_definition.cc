#include <sstream>

#include "function_definition.h"
#include "block.h"
#include "variable.h"

FunctionDefinition::FunctionDefinition() 
        : name_(0)
        , body_(0)
{ }

FunctionDefinition::FunctionDefinition(Variable* name, 
                                       std::vector<Variable*> parameters, 
                                       Block* body) 
        : name_(name)
        , parameters_(parameters)
        , body_(body)
{ }

FunctionDefinition::~FunctionDefinition() {
    if (name_) {
        delete name_;
        name_ = 0;
    }
    int parameters_count = parameters_.size();
    for (int i = 0; i < parameters_count; i++) {
        if (parameters_[i]) {
            delete (parameters_[i]);
            parameters_[i] = 0;
        }
    }
    if (body_) {
        delete body_;
        body_ = 0;
    }
}

std::string FunctionDefinition::ToString() const {
    std::stringstream rv;
    rv << name_->ToString();
    rv << '(';
    int parameters_count = parameters_.size();
    for (int i = 0; i < parameters_count; i++) {
        if (i > 0) {
            rv << ", ";
        }
        rv << (parameters_[i])->ToString();
    }
    rv << ')';
    rv << body_->ToString();
    return rv.str();
}
