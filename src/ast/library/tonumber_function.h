#ifndef MUA_INTERPRETER_AST_LIBRARY_TONUMBER_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_TONUMBER_FUNCTION_H_

#include "../function_definition.h"

class TonumberFunction : public FunctionDefinition 
{
public:
    explicit TonumberFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_TONUMBER_FUNCTION_H_
