#ifndef MUA_INTERPRETER_AST_LIBRARY_TABLE_CONCAT_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_TABLE_CONCAT_BLOCK_H_

#include "../../block.h"

class TableConcatBlock : public Block 
{
public:
    virtual TableConcatBlock* AsTableConcatBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_TABLE_CONCAT_BLOCK_H_
