#include "concat_function.h"
#include "concat_block.h"
#include "../../identifier.h"
#include "../../variable.h"

TableConcatFunction::TableConcatFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("concat"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("table"), index);
    parameters_.push_back(
            new Variable(new Identifier("table"), std::vector<Expression*>()));
    parameters_.push_back(
            new Variable(new Identifier("sep"), std::vector<Expression*>()));
    body_ = new TableConcatBlock();
}
