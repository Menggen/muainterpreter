#ifndef MUA_INTERPRETER_AST_LIBRARY_TABLE_SORT_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_TABLE_SORT_BLOCK_H_

#include "../../block.h"

class TableSortBlock : public Block 
{
public:
    virtual TableSortBlock* AsTableSortBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_TABLE_SORT_BLOCK_H_
