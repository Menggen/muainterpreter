#ifndef MUA_INTERPRETER_AST_LIBRARY_TABLE_CONCAT_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_TABLE_CONCAT_FUNCTION_H_

#include "../../function_definition.h"

class TableConcatFunction : public FunctionDefinition 
{
public:
    explicit TableConcatFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_TABLE_CONCAT_FUNCTION_H_
