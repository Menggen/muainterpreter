#include "sort_function.h"
#include "sort_block.h"
#include "../../identifier.h"
#include "../../variable.h"

TableSortFunction::TableSortFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("sort"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("table"), index);
    parameters_.push_back(
            new Variable(new Identifier("table"), std::vector<Expression*>()));
    parameters_.push_back(
            new Variable(new Identifier("comp"), std::vector<Expression*>()));
    body_ = new TableSortBlock();
}
