#ifndef MUA_INTERPRETER_AST_LIBRARY_TABLE_SORT_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_TABLE_SORT_FUNCTION_H_

#include "../../function_definition.h"

class TableSortFunction : public FunctionDefinition 
{
public:
    explicit TableSortFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_TABLE_SORT_FUNCTION_H_
