#include "tonumber_function.h"
#include "tonumber_block.h"
#include "../identifier.h"
#include "../variable.h"

TonumberFunction::TonumberFunction() {
    std::vector<Expression*> index;
    name_ = new Variable(new Identifier("tonumber"), index);
    parameters_.push_back(
            new Variable(new Identifier("e"), std::vector<Expression*>()));
    body_ = new TonumberBlock();
}
