#ifndef MUA_INTERPRETER_AST_LIBRARY_TOSTRING_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_TOSTRING_FUNCTION_H_

#include "../function_definition.h"

class TostringFunction : public FunctionDefinition 
{
public:
    explicit TostringFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_TOSTRING_FUNCTION_H_
