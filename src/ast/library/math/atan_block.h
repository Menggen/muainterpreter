#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_ATAN_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_ATAN_BLOCK_H_

#include "../../block.h"

class MathAtanBlock : public Block 
{
public:
    virtual MathAtanBlock* AsMathAtanBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_ATAN_BLOCK_H_
