#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_TAN_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_TAN_FUNCTION_H_

#include "../../function_definition.h"

class MathTanFunction : public FunctionDefinition 
{
public:
    explicit MathTanFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_TAN_FUNCTION_H_
