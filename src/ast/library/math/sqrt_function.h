#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_SQRT_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_SQRT_FUNCTION_H_

#include "../../function_definition.h"

class MathSqrtFunction : public FunctionDefinition 
{
public:
    explicit MathSqrtFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_SQRT_FUNCTION_H_
