#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_CEIL_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_CEIL_FUNCTION_H_

#include "../../function_definition.h"

class MathCeilFunction : public FunctionDefinition 
{
public:
    explicit MathCeilFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_CEIL_FUNCTION_H_
