#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_ATAN_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_ATAN_FUNCTION_H_

#include "../../function_definition.h"

class MathAtanFunction : public FunctionDefinition 
{
public:
    explicit MathAtanFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_ATAN_FUNCTION_H_
