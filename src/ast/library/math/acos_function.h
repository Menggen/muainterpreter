#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_ACOS_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_ACOS_FUNCTION_H_

#include "../../function_definition.h"

class MathAcosFunction : public FunctionDefinition 
{
public:
    explicit MathAcosFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_ACOS_FUNCTION_H_
