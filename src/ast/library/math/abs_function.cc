#include "abs_function.h"
#include "abs_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathAbsFunction::MathAbsFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("abs"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathAbsBlock();
}
