#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_SIN_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_SIN_FUNCTION_H_

#include "../../function_definition.h"

class MathSinFunction : public FunctionDefinition 
{
public:
    explicit MathSinFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_SIN_FUNCTION_H_
