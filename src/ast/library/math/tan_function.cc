#include "tan_function.h"
#include "tan_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathTanFunction::MathTanFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("tan"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathTanBlock();
}
