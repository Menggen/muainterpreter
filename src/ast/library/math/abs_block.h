#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_ABS_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_ABS_BLOCK_H_

#include "../../block.h"

class MathAbsBlock : public Block 
{
public:
    virtual MathAbsBlock* AsMathAbsBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_ABS_BLOCK_H_
