#include "log10_function.h"
#include "log10_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathLog10Function::MathLog10Function() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("log10"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathLog10Block();
}
