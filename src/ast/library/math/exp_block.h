#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_EXP_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_EXP_BLOCK_H_

#include "../../block.h"

class MathExpBlock : public Block 
{
public:
    virtual MathExpBlock* AsMathExpBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_EXP_BLOCK_H_
