#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_ACOS_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_ACOS_BLOCK_H_

#include "../../block.h"

class MathAcosBlock : public Block 
{
public:
    virtual MathAcosBlock* AsMathAcosBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_ACOS_BLOCK_H_
