#include "acos_function.h"
#include "acos_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathAcosFunction::MathAcosFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("acos"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathAcosBlock();
}
