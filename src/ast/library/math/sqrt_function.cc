#include "sqrt_function.h"
#include "sqrt_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathSqrtFunction::MathSqrtFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("sqrt"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathSqrtBlock();
}
