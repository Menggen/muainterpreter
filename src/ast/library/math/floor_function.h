#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_FLOOR_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_FLOOR_FUNCTION_H_

#include "../../function_definition.h"

class MathFloorFunction : public FunctionDefinition 
{
public:
    explicit MathFloorFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_FLOOR_FUNCTION_H_
