#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_RAD_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_RAD_BLOCK_H_

#include "../../block.h"

class MathRadBlock : public Block 
{
public:
    virtual MathRadBlock* AsMathRadBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_RAD_BLOCK_H_
