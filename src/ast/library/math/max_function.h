#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_MAX_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_MAX_FUNCTION_H_

#include "../../function_definition.h"

class MathMaxFunction : public FunctionDefinition 
{
public:
    explicit MathMaxFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_MAX_FUNCTION_H_
