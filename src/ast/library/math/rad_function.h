#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_RAD_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_RAD_FUNCTION_H_

#include "../../function_definition.h"

class MathRadFunction : public FunctionDefinition 
{
public:
    explicit MathRadFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_RAD_FUNCTION_H_
