#include "sin_function.h"
#include "sin_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathSinFunction::MathSinFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("sin"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathSinBlock();
}
