#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_ASIN_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_ASIN_FUNCTION_H_

#include "../../function_definition.h"

class MathAsinFunction : public FunctionDefinition 
{
public:
    explicit MathAsinFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_ASIN_FUNCTION_H_
