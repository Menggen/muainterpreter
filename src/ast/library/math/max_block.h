#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_MAX_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_MAX_BLOCK_H_

#include "../../block.h"

class MathMaxBlock : public Block 
{
public:
    virtual MathMaxBlock* AsMathMaxBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_MAX_BLOCK_H_
