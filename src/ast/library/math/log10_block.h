#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_LOG10_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_LOG10_BLOCK_H_

#include "../../block.h"

class MathLog10Block : public Block 
{
public:
    virtual MathLog10Block* AsMathLog10Block() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_LOG10_BLOCK_H_
