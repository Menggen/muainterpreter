#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_LOG10_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_LOG10_FUNCTION_H_

#include "../../function_definition.h"

class MathLog10Function : public FunctionDefinition 
{
public:
    explicit MathLog10Function();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_LOG10_FUNCTION_H_
