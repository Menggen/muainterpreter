#include "cos_function.h"
#include "cos_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathCosFunction::MathCosFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("cos"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathCosBlock();
}
