#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_TAN_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_TAN_BLOCK_H_

#include "../../block.h"

class MathTanBlock : public Block 
{
public:
    virtual MathTanBlock* AsMathTanBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_TAN_BLOCK_H_
