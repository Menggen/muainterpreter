#include "exp_function.h"
#include "exp_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathExpFunction::MathExpFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("exp"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathExpBlock();
}
