#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_COS_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_COS_BLOCK_H_

#include "../../block.h"

class MathCosBlock : public Block 
{
public:
    virtual MathCosBlock* AsMathCosBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_COS_BLOCK_H_
