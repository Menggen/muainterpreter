#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_DEG_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_DEG_FUNCTION_H_

#include "../../function_definition.h"

class MathDegFunction : public FunctionDefinition 
{
public:
    explicit MathDegFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_DEG_FUNCTION_H_
