#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_SQRT_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_SQRT_BLOCK_H_

#include "../../block.h"

class MathSqrtBlock : public Block 
{
public:
    virtual MathSqrtBlock* AsMathSqrtBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_SQRT_BLOCK_H_
