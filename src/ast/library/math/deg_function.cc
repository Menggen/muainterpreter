#include "deg_function.h"
#include "deg_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathDegFunction::MathDegFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("deg"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathDegBlock();
}
