#include "asin_function.h"
#include "asin_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathAsinFunction::MathAsinFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("asin"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathAsinBlock();
}
