#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_MIN_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_MIN_BLOCK_H_

#include "../../block.h"

class MathMinBlock : public Block 
{
public:
    virtual MathMinBlock* AsMathMinBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_MIN_BLOCK_H_
