#include "min_function.h"
#include "min_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathMinFunction::MathMinFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("min"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    parameters_.push_back(
            new Variable(new Identifier("y"), std::vector<Expression*>()));
    body_ = new MathMinBlock();
}
