#include "log_function.h"
#include "log_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathLogFunction::MathLogFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("log"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathLogBlock();
}
