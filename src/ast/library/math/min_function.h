#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_MIN_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_MIN_FUNCTION_H_

#include "../../function_definition.h"

class MathMinFunction : public FunctionDefinition 
{
public:
    explicit MathMinFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_MIN_FUNCTION_H_
