#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_LOG_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_LOG_FUNCTION_H_

#include "../../function_definition.h"

class MathLogFunction : public FunctionDefinition 
{
public:
    explicit MathLogFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_LOG_FUNCTION_H_
