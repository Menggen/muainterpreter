#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_FLOOR_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_FLOOR_BLOCK_H_

#include "../../block.h"

class MathFloorBlock : public Block 
{
public:
    virtual MathFloorBlock* AsMathFloorBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_FLOOR_BLOCK_H_
