#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_ABS_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_ABS_FUNCTION_H_

#include "../../function_definition.h"

class MathAbsFunction : public FunctionDefinition 
{
public:
    explicit MathAbsFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_ABS_FUNCTION_H_
