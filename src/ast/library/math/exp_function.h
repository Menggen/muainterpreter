#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_EXP_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_EXP_FUNCTION_H_

#include "../../function_definition.h"

class MathExpFunction : public FunctionDefinition 
{
public:
    explicit MathExpFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_EXP_FUNCTION_H_
