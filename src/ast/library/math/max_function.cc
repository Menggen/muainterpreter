#include "max_function.h"
#include "max_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathMaxFunction::MathMaxFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("max"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    parameters_.push_back(
            new Variable(new Identifier("y"), std::vector<Expression*>()));
    body_ = new MathMaxBlock();
}
