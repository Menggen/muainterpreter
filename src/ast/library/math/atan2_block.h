#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_ATAN2_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_ATAN2_BLOCK_H_

#include "../../block.h"

class MathAtan2Block : public Block 
{
public:
    virtual MathAtan2Block* AsMathAtan2Block() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_ATAN2_BLOCK_H_
