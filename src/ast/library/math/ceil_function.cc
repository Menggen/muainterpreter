#include "ceil_function.h"
#include "ceil_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathCeilFunction::MathCeilFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("ceil"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathCeilBlock();
}
