#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_ATAN2_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_ATAN2_FUNCTION_H_

#include "../../function_definition.h"

class MathAtan2Function : public FunctionDefinition 
{
public:
    explicit MathAtan2Function();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_ATAN2_FUNCTION_H_
