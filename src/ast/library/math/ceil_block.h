#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_CEIL_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_CEIL_BLOCK_H_

#include "../../block.h"

class MathCeilBlock : public Block 
{
public:
    virtual MathCeilBlock* AsMathCeilBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_CEIL_BLOCK_H_
