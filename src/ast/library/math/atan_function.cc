#include "atan_function.h"
#include "atan_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathAtanFunction::MathAtanFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("atan"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathAtanBlock();
}
