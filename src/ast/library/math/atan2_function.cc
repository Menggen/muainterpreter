#include "atan2_function.h"
#include "atan2_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathAtan2Function::MathAtan2Function() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("atan2"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("y"), std::vector<Expression*>()));
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathAtan2Block();
}
