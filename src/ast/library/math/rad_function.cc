#include "rad_function.h"
#include "rad_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathRadFunction::MathRadFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("rad"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathRadBlock();
}
