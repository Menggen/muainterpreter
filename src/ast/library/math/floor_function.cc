#include "floor_function.h"
#include "floor_block.h"
#include "../../identifier.h"
#include "../../variable.h"

MathFloorFunction::MathFloorFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("floor"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("math"), index);
    parameters_.push_back(
            new Variable(new Identifier("x"), std::vector<Expression*>()));
    body_ = new MathFloorBlock();
}
