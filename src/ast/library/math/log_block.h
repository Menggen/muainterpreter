#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_LOG_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_LOG_BLOCK_H_

#include "../../block.h"

class MathLogBlock : public Block 
{
public:
    virtual MathLogBlock* AsMathLogBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_LOG_BLOCK_H_
