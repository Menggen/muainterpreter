#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_DEG_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_DEG_BLOCK_H_

#include "../../block.h"

class MathDegBlock : public Block 
{
public:
    virtual MathDegBlock* AsMathDegBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_DEG_BLOCK_H_
