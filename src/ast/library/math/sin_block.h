#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_SIN_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_SIN_BLOCK_H_

#include "../../block.h"

class MathSinBlock : public Block 
{
public:
    virtual MathSinBlock* AsMathSinBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_SIN_BLOCK_H_
