#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_COS_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_COS_FUNCTION_H_

#include "../../function_definition.h"

class MathCosFunction : public FunctionDefinition 
{
public:
    explicit MathCosFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_COS_FUNCTION_H_
