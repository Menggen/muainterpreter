#ifndef MUA_INTERPRETER_AST_LIBRARY_MATH_ASIN_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_MATH_ASIN_BLOCK_H_

#include "../../block.h"

class MathAsinBlock : public Block 
{
public:
    virtual MathAsinBlock* AsMathAsinBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_MATH_ASIN_BLOCK_H_
