#ifndef MUA_INTERPRETER_AST_LIBRARY_TONUMBER_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_TONUMBER_BLOCK_H_

#include "../block.h"

class TonumberBlock : public Block 
{
public:
    virtual TonumberBlock* AsTonumberBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_TONUMBER_BLOCK_H_
