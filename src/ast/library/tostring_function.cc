#include "tostring_function.h"
#include "tostring_block.h"
#include "../identifier.h"
#include "../variable.h"

TostringFunction::TostringFunction() {
    std::vector<Expression*> index;
    name_ = new Variable(new Identifier("tostring"), index);
    parameters_.push_back(
            new Variable(new Identifier("e"), std::vector<Expression*>()));
    body_ = new TostringBlock();
}
