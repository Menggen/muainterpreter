#ifndef MUA_INTERPRETER_AST_LIBRARY_PRINT_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_PRINT_BLOCK_H_

#include "../block.h"

class PrintBlock : public Block 
{
public:
    virtual PrintBlock* AsPrintBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_PRINT_BLOCK_H_
