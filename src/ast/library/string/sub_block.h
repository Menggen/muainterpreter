#ifndef MUA_INTERPRETER_AST_LIBRARY_STRING_SUB_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_STRING_SUB_BLOCK_H_

#include "../../block.h"

class StringSubBlock : public Block 
{
public:
    virtual StringSubBlock* AsStringSubBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_STRING_SUB_BLOCK_H_
