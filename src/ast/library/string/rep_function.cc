#include "rep_function.h"
#include "rep_block.h"
#include "../../identifier.h"
#include "../../variable.h"

StringRepFunction::StringRepFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("rep"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("string"), index);
    parameters_.push_back(
            new Variable(new Identifier("s"), std::vector<Expression*>()));
    parameters_.push_back(
            new Variable(new Identifier("n"), std::vector<Expression*>()));
    body_ = new StringRepBlock();
}
