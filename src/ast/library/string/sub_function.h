#ifndef MUA_INTERPRETER_AST_LIBRARY_STRING_SUB_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_STRING_SUB_FUNCTION_H_

#include "../../function_definition.h"

class StringSubFunction : public FunctionDefinition 
{
public:
    explicit StringSubFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_STRING_SUB_FUNCTION_H_
