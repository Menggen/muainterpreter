#include "sub_function.h"
#include "sub_block.h"
#include "../../identifier.h"
#include "../../variable.h"

StringSubFunction::StringSubFunction() {
    std::vector<Expression*> index;
    index.push_back(
            new Variable(new Identifier("sub"), std::vector<Expression*>()));
    name_ = new Variable(new Identifier("string"), index);
    parameters_.push_back(
            new Variable(new Identifier("s"), std::vector<Expression*>()));
    parameters_.push_back(
            new Variable(new Identifier("i"), std::vector<Expression*>()));
    parameters_.push_back(
            new Variable(new Identifier("j"), std::vector<Expression*>()));
    body_ = new StringSubBlock();
}
