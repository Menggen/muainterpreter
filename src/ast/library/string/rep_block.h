#ifndef MUA_INTERPRETER_AST_LIBRARY_STRING_REP_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_STRING_REP_BLOCK_H_

#include "../../block.h"

class StringRepBlock : public Block 
{
public:
    virtual StringRepBlock* AsStringRepBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_STRING_REP_BLOCK_H_
