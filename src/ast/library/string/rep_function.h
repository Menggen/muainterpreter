#ifndef MUA_INTERPRETER_AST_LIBRARY_STRING_REP_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_STRING_REP_FUNCTION_H_

#include "../../function_definition.h"

class StringRepFunction : public FunctionDefinition 
{
public:
    explicit StringRepFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_STRING_REP_FUNCTION_H_
