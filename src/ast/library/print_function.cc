#include "print_function.h"
#include "print_block.h"
#include "../identifier.h"
#include "../variable.h"

PrintFunction::PrintFunction() {
    std::vector<Expression*> index;
    name_ = new Variable(new Identifier("print"), index);
    parameters_.push_back(
            new Variable(new Identifier("e"), std::vector<Expression*>()));
    body_ = new PrintBlock();
}
