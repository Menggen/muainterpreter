#ifndef MUA_INTERPRETER_AST_LIBRARY_PRINT_FUNCTION_H_
#define MUA_INTERPRETER_AST_LIBRARY_PRINT_FUNCTION_H_

#include "../function_definition.h"

class PrintFunction : public FunctionDefinition 
{
public:
    explicit PrintFunction();
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_PRINT_FUNCTION_H_
