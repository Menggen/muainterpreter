#ifndef MUA_INTERPRETER_AST_LIBRARY_TOSTRING_BLOCK_H_
#define MUA_INTERPRETER_AST_LIBRARY_TOSTRING_BLOCK_H_

#include "../block.h"

class TostringBlock : public Block 
{
public:
    virtual TostringBlock* AsTostringBlock() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LIBRARY_TOSTRING_BLOCK_H_
