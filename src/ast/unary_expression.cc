#include "unary_expression.h"

UnaryExpression::UnaryExpression(Token op, Expression* operand) 
        : op_(op)
        , operand_(operand)
{ }

UnaryExpression::~UnaryExpression() {
    if (operand_) {
        delete operand_;
        operand_ = 0;
    }
}

std::string UnaryExpression::ToString() const {
    return op_.ToString() + operand_->ToString();
}
