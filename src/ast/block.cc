#include <sstream>

#include "block.h"
#include "statement.h"

Block::Block() 
{ }

Block::Block(std::vector<Statement*> statements)
        : statements_(statements)
{ }

Block::~Block() {
    int statement_count = statements_.size();
    for (int i = 0; i < statement_count; i++) {
        if (statements_[i]) {
            delete statements_[i];
            statements_[i] = 0;
        }
    }
}

std::string Block::ToString() const {
    std::stringstream rv;
    for (std::vector<Statement*>::const_iterator it = statements_.begin();
            it != statements_.end(); it++)
    {
        rv << (*it)->ToString() << std::endl;
    }
    return rv.str();
}
