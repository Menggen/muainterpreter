#ifndef MUA_INTERPRETER_AST_REPEAT_STATEMENT_H_
#define MUA_INTERPRETER_AST_REPEAT_STATEMENT_H_

#include "control_flow.h"

class Block;
class Expression;

class RepeatStatement : public ControlFlow 
{
public:
    explicit RepeatStatement(Block* body, Expression* condition);
    virtual ~RepeatStatement();

    virtual RepeatStatement* AsRepeatStatement() { return this; }
    virtual std::string ToString() const;
    Block* body() const { return body_; }
    Expression* condition() const { return condition_; }

private:
    Block* body_;
    Expression* condition_;
};

#endif  // MUA_INTERPRETER_AST_REPEAT_STATEMENT_H_