#ifndef MUA_INTERPRETER_AST_BINARY_EXPRESSION_H_
#define MUA_INTERPRETER_AST_BINARY_EXPRESSION_H_

#include "expression.h"
#include "../token.h"

class BinaryExpression : public Expression
{
public:
    explicit BinaryExpression(Expression* left, Token op, Expression* right);
    virtual ~BinaryExpression();

    virtual BinaryExpression* AsBinaryExpression() { return this; }
    virtual std::string ToString() const;
    Expression* left() const { return left_; }
    Token op() const { return op_; }
    Expression* right() const { return right_; }

private:
    Expression* left_;
    Token op_;
    Expression* right_;
};

#endif  // MUA_INTERPRETER_AST_BINARY_EXPRESSION_H_