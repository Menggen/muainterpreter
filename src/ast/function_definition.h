#ifndef MUA_INTERPRETER_AST_FUNCTION_DEFINITION_H_
#define MUA_INTERPRETER_AST_FUNCTION_DEFINITION_H_

#include <vector>

#include "statement.h"

class Block;
class Variable;

class FunctionDefinition : public Statement 
{
public:
    explicit FunctionDefinition();
    explicit FunctionDefinition(Variable* name, 
                                std::vector<Variable*> parameters, 
                                Block* body);
    virtual ~FunctionDefinition();

    virtual FunctionDefinition* AsFunctionDefinition() { return this; }
    virtual std::string ToString() const;
    virtual Variable* name() const { return name_; }
    virtual std::vector<Variable*> parameters() const { return parameters_; }
    virtual Block* body() const { return body_; }

protected:
    Variable* name_;
    std::vector<Variable*> parameters_;
    Block* body_;
};

#endif  // MUA_INTERPRETER_AST_FUNCTION_DEFINITION_H_