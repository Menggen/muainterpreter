#ifndef MUA_INTERPRETER_AST_CONTROL_FLOW_H_
#define MUA_INTERPRETER_AST_CONTROL_FLOW_H_

#include "statement.h"

class ControlFlow : public Statement 
{
public:
    virtual ControlFlow* AsControlFlow() { return this; }
};

#endif  // MUA_INTERPRETER_AST_CONTROL_FLOW_H_