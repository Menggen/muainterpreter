#include "boolean.h"

Boolean::Boolean(bool value) 
        : value_(value)
{ }

std::string Boolean::ToString() const {
    return value_ ? std::string("true") : std::string("false");
}