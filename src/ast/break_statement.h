#ifndef MUA_INTERPRETER_AST_BREAK_STATEMENT_H_
#define MUA_INTERPRETER_AST_BREAK_STATEMENT_H_

#include "last_statement.h"

class BreakStatement : public LastStatement 
{
public:
    virtual BreakStatement* AsBreakStatement() { return this; }
    virtual std::string ToString() const { return "break"; }
};

#endif  // MUA_INTERPRETER_AST_BREAK_STATEMENT_H_