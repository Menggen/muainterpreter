#ifndef MUA_INTERPRETER_AST_EMPTY_TABLE_H_
#define MUA_INTERPRETER_AST_EMPTY_TABLE_H_

#include "simple_expression.h"

class EmptyTable : public SimpleExpression 
{ 
public:
    virtual EmptyTable* AsEmptyTable() { return this; }
    virtual std::string ToString() const { return "{}"; }
};

#endif  // MUA_INTERPRETER_AST_EMPTY_TABLE_H_