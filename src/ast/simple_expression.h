#ifndef MUA_INTERPRETER_AST_SIMPLE_EXPRESSION_H_
#define MUA_INTERPRETER_AST_SIMPLE_EXPRESSION_H_

#include "expression.h"

class SimpleExpression : public Expression 
{ 
public:
    virtual SimpleExpression* AsSimpleExpression() { return this; }
};

#endif  // MUA_INTERPRETER_AST_SIMPLE_EXPRESSION_H_