#ifndef MUA_INTERPRETER_AST_BLOCK_STATEMENT_H_
#define MUA_INTERPRETER_AST_BLOCK_STATEMENT_H_

#include "simple_statement.h"

class Block;

class BlockStatement : public SimpleStatement 
{
public:
    explicit BlockStatement(Block* body);
    virtual ~BlockStatement();

    virtual BlockStatement* AsBlockStatement() { return this; }
    virtual std::string ToString() const;
    Block* body() const { return body_; }

private:
    Block* body_;
};

#endif  // MUA_INTERPRETER_AST_BLOCK_STATEMENT_H_