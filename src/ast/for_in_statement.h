#ifndef MUA_INTERPRETER_AST_FOR_IN_STATEMENT_H_
#define MUA_INTERPRETER_AST_FOR_IN_STATEMENT_H_

#include "control_flow.h"

class Block;
class Variable;

class ForInStatement : public ControlFlow 
{
public:
    explicit ForInStatement(Variable* each, Variable* iterator, Block* body);
    virtual ~ForInStatement();

    virtual ForInStatement* AsForInStatement() { return this; }
    virtual std::string ToString() const;
    Variable* each() const { return each_; }
    Variable* iterator() const { return iterator_; }
    Block* body() const { return body_; }

private:
    Variable* each_;
    Variable* iterator_;
    Block* body_;
};

#endif  // MUA_INTERPRETER_AST_FOR_IN_STATEMENT_H_