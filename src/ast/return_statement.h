#ifndef MUA_INTERPRETER_AST_RETURN_STATEMENT_H_
#define MUA_INTERPRETER_AST_RETURN_STATEMENT_H_

#include "last_statement.h"

class Expression;

class ReturnStatement : public LastStatement 
{
public:
    explicit ReturnStatement();
    explicit ReturnStatement(Expression* value);
    virtual ~ReturnStatement();

    virtual ReturnStatement* AsReturnStatement() { return this; }
    virtual std::string ToString() const;
    Expression* value() const { return value_; }

private:
    Expression* value_;
};

#endif  // MUA_INTERPRETER_AST_RETURN_STATEMENT_H_