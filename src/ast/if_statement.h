#ifndef MUA_INTERPRETER_AST_IF_STATEMENT_H_
#define MUA_INTERPRETER_AST_IF_STATEMENT_H_

#include "control_flow.h"

class Block;
class Expression;

class IfStatement : public ControlFlow 
{
public:
    explicit IfStatement(Expression* condition, 
                         Block* then_body, 
                         Block* else_body);
    virtual ~IfStatement();

    virtual IfStatement* AsIfStatement() { return this; }
    virtual std::string ToString() const;
    Expression* condition() const { return condition_; }
    Block* then_body() const { return then_body_; }
    Block* else_body() const { return else_body_; }

private:
    Expression* condition_;
    Block* then_body_;
    Block* else_body_;
};

#endif  // MUA_INTERPRETER_AST_IF_STATEMENT_H_