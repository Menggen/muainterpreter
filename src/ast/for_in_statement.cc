#include <sstream>

#include "for_in_statement.h"
#include "block.h"
#include "variable.h"

ForInStatement::ForInStatement(Variable* each, 
                               Variable* iterator, 
                               Block* body)
        : each_(each)
        , iterator_(iterator)
        , body_(body)
{ }

ForInStatement::~ForInStatement() {
    if (each_) {
        delete each_;
        each_ = 0;
    }
    if (iterator_) {
        delete iterator_;
        iterator_ = 0;
    }
    if (body_) {
        delete body_;
        body_ = 0;
    }
}

std::string ForInStatement::ToString() const {
    std::stringstream rv;
    rv << "[FORIN] for [EACH] " << each_->ToString() << " in ";
    rv << "[ITERATOR] " << iterator_->ToString() << std::endl;
    rv << "[FORIN] do " << body_->ToString() << "[FORIN] end";
    return rv.str();
}
