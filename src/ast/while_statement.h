#ifndef MUA_INTERPRETER_AST_WHILE_STATEMENT_H_
#define MUA_INTERPRETER_AST_WHILE_STATEMENT_H_

#include "control_flow.h"

class Expression;
class Block;

class WhileStatement : public ControlFlow 
{
public:
    explicit WhileStatement(Expression* condition, Block* body);
    virtual ~WhileStatement();

    virtual WhileStatement* AsWhileStatement() { return this; }
    virtual std::string ToString() const;
    Expression* condition() const { return condition_; }
    Block* body() const { return body_; }

private:
    Expression* condition_;
    Block* body_;
};

#endif  // MUA_INTERPRETER_AST_WHILE_STATEMENT_H_