#ifndef MUA_INTERPRETER_AST_FOR_STATEMENT_H_
#define MUA_INTERPRETER_AST_FOR_STATEMENT_H_

#include "control_flow.h"

class Block;
class Expression;
class Variable;

class ForStatement : public ControlFlow 
{
public:
    explicit ForStatement(Variable* each,
                          Expression* initial_value,
                          Expression* limit_value,
                          Expression* step_value,
                          Block* body);
    virtual ~ForStatement();

    virtual ForStatement* AsForStatement() { return this; }
    virtual std::string ToString() const;
    Variable* each() const { return each_; }
    Expression* initial_value() const { return initial_value_; }
    Expression* limit_value() const { return limit_value_; }
    Expression* step_value() const { return step_value_; }
    Block* body() const { return body_; }

private:
    Variable* each_;
    Expression* initial_value_;
    Expression* limit_value_;
    Expression* step_value_;
    Block* body_;
};

#endif  // MUA_INTERPRETER_AST_FOR_STATEMENT_H_