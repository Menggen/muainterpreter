#include "identifier.h"

Identifier::Identifier(std::string name) 
        : name_(name)
{ }

std::string Identifier::ToString() const {
    return name_;
}
