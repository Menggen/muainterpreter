#include <iostream>
#include <sstream>

#include "expression.h"
#include "function_call.h"
#include "variable.h"

FunctionCall::FunctionCall(Variable* callee, std::vector<Expression*> args)
        : callee_(callee)
        , args_(args)
{ }

FunctionCall::~FunctionCall() {
    if (callee_) {
        delete callee_;
        callee_ = 0;
    }
    int args_count = args_.size();
    for (int i = 0; i < args_count; i++) {
        if (args_[i]) {
            delete (args_[i]);
            args_[i] = 0;
        }
    }
}

std::string FunctionCall::ToString() const {
    std::stringstream rv;
    rv << "[FUNCTION] ";
    rv << callee_->ToString();
    rv << '(';
    int args_count = args_.size();
    for (int i = 0; i < args_count; i++) {
        if (i > 0) {
            rv << ", ";
        }
        Expression* arg = args_[i];
        if (arg) {
            rv << arg->ToString();
        }
        else {
            rv << "(null)";
        }
    }
    rv << ')';
    return rv.str();
}
