#ifndef MUA_INTERPRETER_AST_NODE_H_
#define MUA_INTERPRETER_AST_NODE_H_

#include <string>

// inherited from Node
class Identifier;
class Statement;
class Block;

// inherited from Statement
class SimpleStatement;
class ControlFlow;
class FunctionDefinition;
class LocalVariableDeclaration;
class LastStatement;

// inherited from SimpleStatement
class EmptyStatement;
class AssignmentStatement;
class FunctionCall;
class BlockStatement;
class Expression;

// inherited from ControlFlow
class WhileStatement;
class RepeatStatement;
class IfStatement;
class ForStatement;
class ForInStatement;

// inherited from LastStatement
class ReturnStatement;
class BreakStatement;

// inherited from Expression
class SimpleExpression;
class BinaryExpression;
class UnaryExpression;
class ParenExpression;

// inherited from SimpleExpression
class Number;
class String;
class Nil;
class Boolean;
class EmptyTable;
class Variable;

class Node 
{
public:
    virtual ~Node() { }
    
    // inherited from Node
    virtual Identifier* AsIdentifier() { return 0; }
    virtual Statement* AsStatement() { return 0; }
    virtual Block* AsBlock() { return 0; }

    // inherited from Statement
    virtual SimpleStatement* AsSimpleStatement() { return 0; }
    virtual ControlFlow* AsControlFlow() { return 0; }
    virtual FunctionDefinition* AsFunctionDefinition() { return 0; }
    virtual LocalVariableDeclaration* AsLocalVariableDeclaration() { 
        return 0; 
    }
    virtual LastStatement* AsLastStatement() { return 0; }

    // inherited from SimpleStatement
    virtual EmptyStatement* AsEmptyStatement() { return 0; }
    virtual AssignmentStatement* AsAssignmentStatement() { return 0; }
    virtual FunctionCall* AsFunctionCall() { return 0; }
    virtual BlockStatement* AsBlockStatement() { return 0; }
    virtual Expression* AsExpression() { return 0; }

    // inherited from ControlFlow
    virtual WhileStatement* AsWhileStatement() { return 0; }
    virtual RepeatStatement* AsRepeatStatement() { return 0; }
    virtual IfStatement* AsIfStatement() { return 0; }
    virtual ForStatement* AsForStatement() { return 0; }
    virtual ForInStatement* AsForInStatement() { return 0; }

    // inherited from LastStatement
    virtual ReturnStatement* AsReturnStatement() { return 0; }
    virtual BreakStatement* AsBreakStatement() { return 0; }

    // inherited from Expression
    virtual SimpleExpression* AsSimpleExpression() { return 0; }
    virtual BinaryExpression* AsBinaryExpression() { return 0; }
    virtual UnaryExpression* AsUnaryExpression() { return 0; }
    virtual ParenExpression* AsParenExpression() { return 0; }

    // inherited from SimpleExpression
    virtual Number* AsNumber() { return 0; }
    virtual String* AsString() { return 0; }
    virtual Nil* AsNil() { return 0; }
    virtual Boolean* AsBoolean() { return 0; }
    virtual EmptyTable* AsEmptyTable() { return 0; }
    virtual Variable* AsVariable() { return 0; }

    virtual std::string ToString() const { return std::string(); }
};

#endif  // MUA_INTERPRETER_AST_NODE_H_