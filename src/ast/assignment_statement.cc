#include <sstream>

#include "assignment_statement.h"
#include "variable.h"
#include "expression.h"

AssignmentStatement::AssignmentStatement(Variable* target, Expression* value)
        : target_(target)
        , value_(value)
{ }

AssignmentStatement::~AssignmentStatement() {
    if (target_) {
        delete target_;
        target_ = 0;
    }
    if (value_) {
        delete value_;
        value_ = 0;    
    }
}

std::string AssignmentStatement::ToString() const {
    std::stringstream rv;
    rv << "[ASSIGNMENT] "; 
    rv << target_->ToString();
    rv << " = ";
    rv << value_->ToString();
    return rv.str();
}