#include <sstream>

#include "block.h"
#include "expression.h"
#include "repeat_statement.h"

RepeatStatement::RepeatStatement(Block* body, Expression* condition)
        : body_(body)
        , condition_(condition)
{ }

RepeatStatement::~RepeatStatement() {
    if (body_) {
        delete body_;
        body_ = 0;
    }
    if (condition_) {
        delete condition_;
        condition_ = 0;
    }
}

std::string RepeatStatement::ToString() const {
    std::stringstream rv;
    rv << "repeat " << body_->ToString() << "until " << condition_->ToString();
    return rv.str();
}
