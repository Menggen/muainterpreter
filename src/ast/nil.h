#ifndef MUA_INTERPRETER_AST_NIL_H_
#define MUA_INTERPRETER_AST_NIL_H_

#include "simple_expression.h"

class Nil : public SimpleExpression 
{
public:
    virtual Nil* AsNil() { return this; }
    virtual std::string ToString() const { return "nil"; }
};

#endif  // MUA_INTERPRETER_AST_NIL_H_