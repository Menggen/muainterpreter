#include <sstream>

#include "block.h"
#include "block_statement.h"

BlockStatement::BlockStatement(Block* body)
        : body_(body)
{ }

BlockStatement::~BlockStatement() {
    if (body_) {
        delete body_;
        body_ = 0;
    }
}

std::string BlockStatement::ToString() const {
    std::stringstream rv;
    rv << "do" << body_->ToString() << "end";
    return rv.str();
}
