#ifndef MUA_INTERPRETER_AST_IDENTIFIER_H_
#define MUA_INTERPRETER_AST_IDENTIFIER_H_

#include <string>

#include "node.h"

class Identifier : public Node
{
public:
    explicit Identifier(std::string name);

    virtual Identifier* AsIdentifier() { return this; }
    virtual std::string ToString() const;
    std::string name() const { return name_; }

private:
    std::string name_;
};

#endif  // MUA_INTERPRETER_AST_IDENTIFIER_H_