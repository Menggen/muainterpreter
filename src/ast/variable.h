#ifndef MUA_INTERPRETER_AST_VARIABLE_H_
#define MUA_INTERPRETER_AST_VARIABLE_H_

#include <string>
#include <vector>

#include "simple_expression.h"

class Expression;
class Identifier;

class Variable : public SimpleExpression
{
public:
    explicit Variable(Identifier* name, std::vector<Expression*> index);
    explicit Variable(Identifier* name);
    virtual ~Variable();

    virtual Variable* AsVariable() { return this; }
    virtual std::string ToString() const;
    Identifier* name() const { return name_; }
    std::vector<Expression*> index() const { return index_; }
    void SetIndex(std::vector<Expression*> index);

private:
    Identifier* name_;
    std::vector<Expression*> index_;
};

#endif  // MUA_INTERPRETER_AST_VARIABLE_H_