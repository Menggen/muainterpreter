#include "binary_expression.h"

BinaryExpression::BinaryExpression(Expression* left, 
                                   Token op, 
                                   Expression* right)
        : left_(left)
        , op_(op)
        , right_(right)
{ }  
        
BinaryExpression::~BinaryExpression() {
    if (left_) {
        delete left_;
        left_ = 0;
    }
    if (right_) {
        delete right_;
        right_ = 0;
    }
}

std::string BinaryExpression::ToString() const {
    return left_->ToString() + " " + op_.ToString() + " " + right_->ToString();
}
