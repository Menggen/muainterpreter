#include <sstream>

#include "block.h"
#include "expression.h"
#include "while_statement.h"

WhileStatement::WhileStatement(Expression* condition, Block* body)
        : condition_(condition)
        , body_(body)
{ }

WhileStatement::~WhileStatement() {
    if (condition_) {
        delete condition_;
        condition_ = 0;
    }
    if (body_) {
        delete body_;
        body_ = 0;
    }
}

std::string WhileStatement::ToString() const {
    std::stringstream rv;
    rv << "while " << condition_->ToString() << std::endl;
    rv << "do" << body_->ToString() << "end" << std::endl;
    return rv.str();
}
