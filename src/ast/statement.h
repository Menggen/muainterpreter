#ifndef MUA_INTERPRETER_AST_STATEMENT_H_
#define MUA_INTERPRETER_AST_STATEMENT_H_

#include "node.h"

class Statement : public Node 
{
public:
    virtual Statement* AsStatement() { return this; }
};

#endif  // MUA_INTERPRETER_AST_STATEMENT_H_