#ifndef MUA_INTERPRETER_AST_LOCAL_VARIABLE_DECLARATION_H_
#define MUA_INTERPRETER_AST_LOCAL_VARIABLE_DECLARATION_H_

#include "statement.h"

class Expression;
class Variable;

class LocalVariableDeclaration : public Statement 
{
public:
    explicit LocalVariableDeclaration(Variable* name, Expression* value);
    ~LocalVariableDeclaration();

    virtual LocalVariableDeclaration* AsLocalVariableDeclaration() { 
        return this; 
    }
    virtual std::string ToString() const;
    bool HasValue() const { return value_ != 0; }
    Variable* name() const { return name_; }
    Expression* value() const { return value_; }

private:
    Variable* name_;
    Expression* value_;
};

#endif  // MUA_INTERPRETER_AST_LOCAL_VARIABLE_DECLARATION_H_