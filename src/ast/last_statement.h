#ifndef MUA_INTERPRETER_AST_LAST_STATEMENT_H_
#define MUA_INTERPRETER_AST_LAST_STATEMENT_H_

#include "statement.h"

class LastStatement : public Statement 
{
public:
    virtual LastStatement* AsLastStatement() { return this; }
};

#endif  // MUA_INTERPRETER_AST_LAST_STATEMENT_H_