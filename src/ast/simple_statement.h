#ifndef MUA_INTERPRETER_AST_SIMPLE_STATEMENT_H_
#define MUA_INTERPRETER_AST_SIMPLE_STATEMENT_H_

#include "statement.h"

class SimpleStatement : public Statement 
{
public:
    virtual SimpleStatement* AsSimpleStatement() { return this; }
};

#endif  // MUA_INTERPRETER_AST_SIMPLE_STATEMENT_H_