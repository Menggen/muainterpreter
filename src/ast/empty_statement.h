#ifndef MUA_INTERPRETER_AST_EMPTY_STATEMENT_H_
#define MUA_INTERPRETER_AST_EMPTY_STATEMENT_H_

#include "simple_statement.h"

class EmptyStatement : public SimpleStatement 
{
public:
    virtual EmptyStatement* AsEmptyStatement() { return this; }
};

#endif  // MUA_INTERPRETER_AST_EMPTY_STATEMENT_H_