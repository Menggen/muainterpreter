#include <sstream>

#include "for_statement.h"
#include "block.h"
#include "expression.h"
#include "variable.h"

ForStatement::ForStatement(Variable* each,
                           Expression* initial_value,
                           Expression* limit_value,
                           Expression* step_value,
                           Block* body)
        : each_(each)
        , initial_value_(initial_value)
        , limit_value_(limit_value)
        , step_value_(step_value)
        , body_(body)
{ }

ForStatement::~ForStatement() {
    if (each_) {
        delete each_;
        each_ = 0;
    }
    if (initial_value_) {
        delete initial_value_;
        initial_value_ = 0;
    }
    if (limit_value_) {
        delete limit_value_;
        limit_value_ = 0;
    }
    if (step_value_) {
        delete step_value_;
        step_value_ = 0;
    }
    if (body_) {
        delete body_;
        body_ = 0;
    }
}

std::string ForStatement::ToString() const {
    std::stringstream rv;
    rv << "[FOR] for [INIT] " << each_->ToString() << " = ";
    rv << initial_value_->ToString() << ", ";
    rv << "[LIMIT] " << limit_value_->ToString() << ", ";
    rv << "[STEP] " << step_value_->ToString() << std::endl;
    rv << "[FOR] do " << body_->ToString() << "[FOR] end";
    return rv.str();
}
