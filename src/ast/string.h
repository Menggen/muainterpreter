#ifndef MUA_INTERPRETER_AST_STRING_H_
#define MUA_INTERPRETER_AST_STRING_H_

#include <string>

#include "simple_expression.h"

class String : public SimpleExpression
{
public:
    explicit String(std::string value);

    virtual String* AsString() { return this; }
    virtual std::string ToString() const { return value_; }

    std::string value() const { return value_; }

private:
    std::string value_;
};

#endif  // MUA_INTERPRETER_AST_STRING_H_