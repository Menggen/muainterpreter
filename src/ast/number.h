#ifndef MUA_INTERPRETER_AST_NUMBER_H_
#define MUA_INTERPRETER_AST_NUMBER_H_

#include <string>

#include "simple_expression.h"

class Number : public SimpleExpression
{
public:
    explicit Number(double value);

    virtual Number* AsNumber() { return this; }
    virtual std::string ToString() const;

    double value() const { return value_; }

private:
    double value_;
};

#endif  // MUA_INTERPRETER_AST_NUMBER_H_