#ifndef MUA_INTERPRETER_AST_PAREN_EXPRESSION_H_
#define MUA_INTERPRETER_AST_PAREN_EXPRESSION_H_

#include "expression.h"

class ParenExpression : public Expression
{
public:
    explicit ParenExpression(Expression* value);
    virtual ~ParenExpression();

    virtual ParenExpression* AsParenExpression() { return this; }
    virtual std::string ToString() const;
    Expression* value() const { return value_; }

private:
    Expression* value_;
};

#endif  // MUA_INTERPRETER_AST_PAREN_EXPRESSION_H_