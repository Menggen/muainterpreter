#ifndef MUA_INTERPRETER_AST_EXPRESSION_H_
#define MUA_INTERPRETER_AST_EXPRESSION_H_

#include "simple_statement.h"

class Expression : public SimpleStatement 
{
public:
    virtual Expression* AsExpression() { return this; }
};

#endif  // MUA_INTERPRETER_AST_EXPRESSION_H_