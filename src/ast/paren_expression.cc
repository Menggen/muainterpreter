#include "paren_expression.h"

ParenExpression::ParenExpression(Expression* value) 
        : value_(value)
{ }

ParenExpression::~ParenExpression() {
    if (value_) {
        delete value_;
        value_ = 0;
    }
}

std::string ParenExpression::ToString() const {
    return "(" + value_->ToString() + ")";
}
