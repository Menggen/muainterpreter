#ifndef MUA_INTERPRETER_AST_ASSIGNMENT_STATEMENT_H_
#define MUA_INTERPRETER_AST_ASSIGNMENT_STATEMENT_H_

#include <string>

#include "simple_statement.h"

class Expression;
class Variable;

class AssignmentStatement : public SimpleStatement
{
public:
    explicit AssignmentStatement(Variable* target, Expression* value);
    virtual ~AssignmentStatement();

    virtual AssignmentStatement* AsAssignmentStatement() { return this; }
    virtual std::string ToString() const;

    Variable* target() const { return target_; }
    Expression* value() const { return value_; }

private:
    Variable* target_;
    Expression* value_;
};

#endif  // MUA_INTERPRETER_AST_ASSIGNMENT_STATEMENT_H_