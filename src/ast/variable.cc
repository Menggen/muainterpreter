#include <sstream>

#include "identifier.h"
#include "variable.h"

Variable::Variable(Identifier* name, std::vector<Expression*> index)
        : name_(name)
        , index_(index)
{ }

Variable::Variable(Identifier* name)
        : name_(name)
{ }

Variable::~Variable() {
    if (name_) {
        delete name_;
        name_ = 0;
    }
}

std::string Variable::ToString() const {
    std::stringstream rv;
    rv << name_->ToString();
    int index_count = index_.size();
    for (int i = 0; i < index_count; i++) {
        Expression* index = index_[i];
        if (index) {
            rv << "[" << index->ToString() << "]";
        }
    }
    return rv.str();
}

void Variable::SetIndex(std::vector<Expression*> index) { 
    index_ = index; 
}