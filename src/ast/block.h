#ifndef MUA_INTERPRETER_AST_BLOCK_H_
#define MUA_INTERPRETER_AST_BLOCK_H_

#include <vector>

#include "node.h"

class Statement;

class PrintBlock;
class TonumberBlock;
class TostringBlock;
class StringRepBlock;
class StringSubBlock;
class TableConcatBlock;
class TableSortBlock;
class MathAbsBlock;
class MathFloorBlock;
class MathCeilBlock;
class MathSqrtBlock;
class MathExpBlock;
class MathLogBlock;
class MathLog10Block;
class MathRadBlock;
class MathDegBlock;
class MathAcosBlock;
class MathAsinBlock;
class MathAtanBlock;
class MathAtan2Block;
class MathCosBlock;
class MathSinBlock;
class MathTanBlock;
class MathMinBlock;
class MathMaxBlock;

class Block : public Node 
{
public:
    explicit Block();
    explicit Block(std::vector<Statement*> statements);
    virtual ~Block();

    virtual Block* AsBlock() { return this; }
    
    // Library functions
    virtual PrintBlock* AsPrintBlock() { return 0; }

    virtual TonumberBlock* AsTonumberBlock() { return 0; }
    virtual TostringBlock* AsTostringBlock() { return 0; }

    virtual StringRepBlock* AsStringRepBlock() { return 0; }
    virtual StringSubBlock* AsStringSubBlock() { return 0; }

    virtual TableConcatBlock* AsTableConcatBlock() { return 0; }
    virtual TableSortBlock* AsTableSortBlock() { return 0; }

    virtual MathAbsBlock* AsMathAbsBlock() { return 0; }
    virtual MathFloorBlock* AsMathFloorBlock() { return 0; }
    virtual MathCeilBlock* AsMathCeilBlock() { return 0; }

    virtual MathSqrtBlock* AsMathSqrtBlock() { return 0; }
    virtual MathExpBlock* AsMathExpBlock() { return 0; }
    virtual MathLogBlock* AsMathLogBlock() { return 0; }
    virtual MathLog10Block* AsMathLog10Block() { return 0; }

    virtual MathRadBlock* AsMathRadBlock() { return 0; }
    virtual MathDegBlock* AsMathDegBlock() { return 0; }

    virtual MathAcosBlock* AsMathAcosBlock() { return 0; }
    virtual MathAsinBlock* AsMathAsinBlock() { return 0; }
    virtual MathAtanBlock* AsMathAtanBlock() { return 0; }
    virtual MathAtan2Block* AsMathAtan2Block() { return 0; }

    virtual MathCosBlock* AsMathCosBlock() { return 0; }
    virtual MathSinBlock* AsMathSinBlock() { return 0; }
    virtual MathTanBlock* AsMathTanBlock() { return 0; }

    virtual MathMinBlock* AsMathMinBlock() { return 0; }
    virtual MathMaxBlock* AsMathMaxBlock() { return 0; }

    virtual std::string ToString() const;
    std::vector<Statement*> statements() { return statements_; }

private:
    std::vector<Statement*> statements_;
};

#endif  // MUA_INTERPRETER_AST_BLOCK_H_