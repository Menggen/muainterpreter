#include <sstream>

#include "number.h"

Number::Number(double value) 
        : value_(value)
{ }

std::string Number::ToString() const {
    std::stringstream rv;
    rv << value_;
    return rv.str();
}