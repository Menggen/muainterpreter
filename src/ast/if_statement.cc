#include <sstream>

#include "block.h"
#include "expression.h"
#include "if_statement.h"

IfStatement::IfStatement(Expression* condition, 
                         Block* then_body, 
                         Block* else_body)
        : condition_(condition)
        , then_body_(then_body)
        , else_body_(else_body)
{ }

IfStatement::~IfStatement() {
    if (condition_) {
        delete condition_;
        condition_ = 0;
    }
    if (then_body_) {
        delete then_body_;
        then_body_ = 0;
    }
    if (else_body_) {
        delete else_body_;
        else_body_ = 0;
    }
}

std::string IfStatement::ToString() const {
    std::stringstream rv;
    rv << "[IF] if " << condition_->ToString() << std::endl;
    rv << "[IF] then " << then_body_->ToString();
    rv << "[IF] else " << else_body_->ToString();
    return rv.str();
}
