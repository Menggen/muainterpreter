#include "expression.h"
#include "return_statement.h"

ReturnStatement::ReturnStatement() 
        : value_(0)
{ }

ReturnStatement::ReturnStatement(Expression* value) 
        : value_(value)
{ }

ReturnStatement::~ReturnStatement() {
    if (value_) {
        delete value_;
        value_ = 0;
    }
}

std::string ReturnStatement::ToString() const {
    return "return " + value_->ToString();
}