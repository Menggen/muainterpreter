#ifndef MUA_INTERPRETER_AST_FUNCTION_CALL_H_
#define MUA_INTERPRETER_AST_FUNCTION_CALL_H_

#include <vector>

#include "simple_expression.h"

class Expression;
class Variable;

class FunctionCall : public SimpleExpression
{
public:
    explicit FunctionCall(Variable* callee, std::vector<Expression*> args);
    virtual ~FunctionCall();

    virtual FunctionCall* AsFunctionCall() { return this; }
    virtual std::string ToString() const;
    Variable* callee() const { return callee_; }
    std::vector<Expression*> args() const { return args_; }

private:
    Variable* callee_;
    std::vector<Expression*> args_;
};

#endif  // MUA_INTERPRETER_AST_FUNCTION_CALL_H_