#ifndef MUA_INTERPRETER_AST_BOOLEAN_H_
#define MUA_INTERPRETER_AST_BOOLEAN_H_

#include "simple_expression.h"

class Boolean : public SimpleExpression 
{ 
public:
    explicit Boolean(bool value);

    virtual Boolean* AsBoolean() { return this; }
    virtual std::string ToString() const;

    bool value() const { return value_; }

private:
    bool value_;
};

#endif  // MUA_INTERPRETER_AST_BOOLEAN_H_