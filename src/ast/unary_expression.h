#ifndef MUA_INTERPRETER_AST_UNARY_EXPRESSION_H_
#define MUA_INTERPRETER_AST_UNARY_EXPRESSION_H_

#include "expression.h"
#include "../token.h"

class UnaryExpression : public Expression
{
public:
    explicit UnaryExpression(Token op, Expression* operand);
    virtual ~UnaryExpression();

    virtual UnaryExpression* AsUnaryExpression() { return this; }
    virtual std::string ToString() const;
    Expression* operand() const { return operand_; }
    Token op() const { return op_; }

private:
    Token op_;
    Expression* operand_;
};

#endif  // MUA_INTERPRETER_AST_UNARY_EXPRESSION_H_