#ifndef MUA_INTERPRETER_INTERPRETER_H_
#define MUA_INTERPRETER_INTERPRETER_H_

class Source;
class Node;
class SymbolTable;
class RuntimeStack;

// inherited from Node
class Block;
class Statement;

// inherited from Statement
class SimpleStatement;
class ControlFlow;
class FunctionDefinition;
class LocalVariableDeclaration;
class LastStatement;

// inherited from SimpleStatement
class EmptyStatement;
class AssignmentStatement;
class FunctionCall;
class BlockStatement;
class Expression;

// inherited from ControlFlow
class WhileStatement;
class RepeatStatement;
class IfStatement;
class ForStatement;
class ForInStatement;

// inherited from LastStatement
class ReturnStatement;
class BreakStatement;

// inherited from Expression
class SimpleExpression;
class BinaryExpression;
class UnaryExpression;
class ParenExpression;

// inherited from SimpleExpression
class Number;
class String;
class Nil;
class Boolean;
class EmptyTable;
class Variable;

// Library function blocks
class PrintBlock;
class TonumberBlock;
class TostringBlock;
class StringRepBlock;
class StringSubBlock;
class TableConcatBlock;
class TableSortBlock;
class MathAbsBlock;
class MathFloorBlock;
class MathCeilBlock;
class MathSqrtBlock;
class MathExpBlock;
class MathLogBlock;
class MathLog10Block;
class MathRadBlock;
class MathDegBlock;
class MathAcosBlock;
class MathAsinBlock;
class MathAtanBlock;
class MathAtan2Block;
class MathCosBlock;
class MathSinBlock;
class MathTanBlock;
class MathMinBlock;
class MathMaxBlock;

class Interpreter
{
public:
    explicit Interpreter(Source& source);
    ~Interpreter();

    void Interpret();

private:
    void ImportLibrary();
    void Execute(Block* block);
    void ExecuteRegularBlock(Block* block);
    void Execute(Statement* statement);

    // Statement execution
    void Execute(SimpleStatement* statement);
    void Execute(FunctionDefinition* statement);
    void Execute(LocalVariableDeclaration* statement);

    // SimpleStatement execution
    void Execute(EmptyStatement* statement);
    void Execute(AssignmentStatement* statement);
    void Execute(FunctionCall* statement);
    void Execute(BlockStatement* statement);

    // ControlFlow execution
    void Execute(WhileStatement* statement);
    void Execute(RepeatStatement* statement);
    void Execute(IfStatement* statement);
    void Execute(ForStatement* statement);
    bool IsFalseConditional(Expression* expression);

    void Execute(ReturnStatement* statement);
    void Execute(BreakStatement* statement);

    // Library function block execution
    void Execute(PrintBlock* block);
    void Execute(TonumberBlock* block);
    void Execute(TostringBlock* block);
    void Execute(StringRepBlock* block);
    void Execute(StringSubBlock* block);
    void Execute(TableConcatBlock* block);
    void Execute(TableSortBlock* block);
    void Execute(MathAbsBlock* block);
    void Execute(MathFloorBlock* block);
    void Execute(MathCeilBlock* block);
    void Execute(MathSqrtBlock* block);
    void Execute(MathExpBlock* block);
    void Execute(MathLogBlock* block);
    void Execute(MathLog10Block* block);
    void Execute(MathRadBlock* block);
    void Execute(MathDegBlock* block);
    void Execute(MathAcosBlock* block);
    void Execute(MathAsinBlock* block);
    void Execute(MathAtanBlock* block);
    void Execute(MathAtan2Block* block);
    void Execute(MathCosBlock* block);
    void Execute(MathSinBlock* block);
    void Execute(MathTanBlock* block);
    void Execute(MathMinBlock* block);
    void Execute(MathMaxBlock* block);
    Node* GetFunctionArgValue(const std::string& name);

    // SimpleExpression evaluation
    SimpleExpression* Evaluate(Expression* expression);
    SimpleExpression* Evaluate(SimpleExpression* expression);
    SimpleExpression* Evaluate(BinaryExpression* expression);
    SimpleExpression* Evaluate(UnaryExpression* expression);
    SimpleExpression* Evaluate(ParenExpression* expression);
    SimpleExpression* Evaluate(Variable* expression);
    Variable* EvaluateIndex(Variable* expression);
    SimpleExpression* Evaluate(FunctionCall* expression);
    void FillFunctionCallArgs(FunctionCall* call, 
                              FunctionDefinition* definition);
    

    // UnaryExpression and BinaryExpression evaluation
    Boolean* Not(SimpleExpression* operand);
    Number* Minus(SimpleExpression* operand);
    Number* StringLength(SimpleExpression* operand);
    Number* TableLength(Variable* operand);
    Boolean* And(SimpleExpression* left, SimpleExpression* right);
    Boolean* Or(SimpleExpression* left, SimpleExpression* right);
    Boolean* Less(SimpleExpression* left, SimpleExpression* right);
    Boolean* Greater(SimpleExpression* left, SimpleExpression* right);
    Boolean* LessEqual(SimpleExpression* left, SimpleExpression* right);
    Boolean* GreaterEqual(SimpleExpression* left, SimpleExpression* right);
    Boolean* NotEqual(SimpleExpression* left, SimpleExpression* right);
    Boolean* Equal(SimpleExpression* left, SimpleExpression* right);
    String* Concatenate(SimpleExpression* left, SimpleExpression* right);
    Number* Plus(SimpleExpression* left, SimpleExpression* right);
    Number* Minus(SimpleExpression* left, SimpleExpression* right);
    Number* Multiple(SimpleExpression* left, SimpleExpression* right);
    Number* Divide(SimpleExpression* left, SimpleExpression* right);
    Number* Mod(SimpleExpression* left, SimpleExpression* right);
    Number* Pow(SimpleExpression* left, SimpleExpression* right);

    Source& source_;
    SymbolTable* symbol_table_;
    RuntimeStack* runtime_stack_;
};

#endif  // MUA_INTERPRETER_INTERPRETER_H_