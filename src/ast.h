#ifndef MUA_INTERPRETER_AST_H_
#define MUA_INTERPRETER_AST_H_

// abstract syntax tree node
#include "ast/node.h"

// inherited from Node
#include "ast/identifier.h"
#include "ast/statement.h"
#include "ast/block.h"

// inherited from Statement
#include "ast/simple_statement.h"
#include "ast/control_flow.h"
#include "ast/function_definition.h"
#include "ast/local_variable_declaration.h"
#include "ast/last_statement.h"

// inherited from SimpleStatement
#include "ast/empty_statement.h"
#include "ast/assignment_statement.h"
#include "ast/function_call.h"
#include "ast/block_statement.h"
#include "ast/expression.h"

// inherited from ControlFlow
#include "ast/while_statement.h"
#include "ast/repeat_statement.h"
#include "ast/if_statement.h"
#include "ast/for_statement.h"
#include "ast/for_in_statement.h"

// inherited from LastStatement
#include "ast/return_statement.h"
#include "ast/break_statement.h"

// inherited from Expression
#include "ast/simple_expression.h"
#include "ast/binary_expression.h"
#include "ast/unary_expression.h"
#include "ast/paren_expression.h"

// inherited from SimpleExpression
#include "ast/number.h"
#include "ast/string.h"
#include "ast/nil.h"
#include "ast/boolean.h"
#include "ast/empty_table.h"
#include "ast/variable.h"

// Library functions
#include "ast/library.h"

#endif  // MUA_INTERPRETER_AST_H_
