#ifndef MUA_INTERPRETER_PARSER_H_
#define MUA_INTERPRETER_PARSER_H_

#include <stack>
#include <vector>

#include "token.h"

class Source;
class Scanner;

// inherited from Node
class Identifier;
class Block;
class Statement;

// inherited from Statement
class SimpleStatement;
class ControlFlow;
class FunctionDefinition;
class LocalVariableDeclaration;
class LastStatement;

// inherited from SimpleStatement
class EmptyStatement;
class AssignmentStatement;
class FunctionCall;
class BlockStatement;
class Expression;

// inherited from ControlFlow
class WhileStatement;
class RepeatStatement;
class IfStatement;
class ForStatement;
class ForInStatement;

// inherited from LastStatement
class ReturnStatement;
class BreakStatement;

// inherited from Expression
class SimpleExpression;
class BinaryExpression;
class UnaryExpression;
class ParenExpression;

// inherited from SimpleExpression
class Number;
class String;
class Nil;
class Boolean;
class EmptyTable;
class Variable;

class Parser 
{
public:
    explicit Parser(Source& source);
    ~Parser();

    void Parse();
    Block* ast() const { return ast_; }
    void PrintAbstractSyntaxTree() const;

private:
    // Node
    Identifier* ParseIdentifier();
    Block* ParseBlock();
    Statement* ParseStatement();

    // Statement
    SimpleStatement* ParseSimpleStatement();
    ControlFlow* ParseControlFlow();
    FunctionDefinition* ParseFunctionDefinition();
    LocalVariableDeclaration* ParseLocalVariableDeclaration();
    LastStatement* ParseLastStatement();

    // SimpleStatement
    EmptyStatement* ParseEmptyStatement();
    AssignmentStatement* ParseAssignmentStatement(Variable* parsed_target);
    FunctionCall* ParseFunctionCall(Variable* parsed_callee);
    BlockStatement* ParseBlockStatement();
    
    // Expression
    Expression* ParseExpression();
    Expression* ParseExpressionImpl(Expression* left, int precedence);
    Expression* ParsePrimaryExpression();
    SimpleExpression* ParseSimpleExpression();
    UnaryExpression* ParseUnaryExpression();
    ParenExpression* ParseParenExpression();

    // SimpleExpression
    Number* ParseNumber();
    String* ParseString();
    Nil* ParseNil();
    Boolean* ParseBoolean(TokenType parsed_type);
    EmptyTable* ParseEmptyTable();
    Variable* ParseVariable();

    // ControlFlow
    WhileStatement* ParseWhileStatement();
    RepeatStatement* ParseRepeatStatement();
    IfStatement* ParseIfStatement();
    ForStatement* ParseForStatement(Variable* parsed_each);
    ForInStatement* ParseForInStatement(Variable* parsed_each);

    // ParseIfStatement Helper
    void ParseIfStatementConditionBodyStack(        
            std::stack<Expression*>* condition_stack,
            std::stack<Block*>* then_stack);
    Block* ParseIfStatementElseBody();
    IfStatement* CreateNestedIfStatement(
            std::stack<Expression*>* condition_stack,
            std::stack<Block*>* then_stack,
            Block* else_body);

    // LastStatement
    ReturnStatement* ParseReturnStatement();
    BreakStatement* ParseBreakStatement();

    // Helper
    Token GetCurrentToken();
    void ConsumeToken();
    Block* CreateBlock(Statement* statement);

    Source& source_;
    Scanner* scanner_;
    Block* ast_;
};

#endif  // MUA_INTERPRETER_PARSER_H_