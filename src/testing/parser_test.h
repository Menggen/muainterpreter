#ifndef MUA_INTERPRETER_TESTING_PARSER_TEST_H_
#define MUA_INTERPRETER_TESTING_PARSER_TEST_H_

#include <string>

class ParserTest
{
public:
    static void Run();

private:
    static void Parse(const std::string& code);
    static void ParseWithDebug(const std::string& code);
};

#endif  // MUA_INTERPRETER_TESTING_PARSER_TEST_H_