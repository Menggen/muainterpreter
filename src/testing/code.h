#ifndef MUA_INTERPRETER_TESTING_CODE_H_
#define MUA_INTERPRETER_TESTING_CODE_H_

#include <string>

class Code
{
public:
    static std::string GetSampleInputCode();
    static std::string GetNewtonMethodCode();
    static std::string GetSuperEasyCode();
    static std::string GetSimpleStatementCode();
    static std::string GetWhileStatementCode();
    static std::string GetRepeatStatementCode();
    static std::string GetLocalVariableDeclarationCode();
    static std::string GetIfStatementCode();
    static std::string GetBlockStatementCode();
    static std::string GetFunctionDefinitionCode();
    static std::string GetForInStatementCode();
    static std::string GetSqrt3Code();
    static std::string GetSumFrom1To10Code();
    static std::string GetBreakableWhileStatementCode();
    static std::string GetCollatzConjectureCode();
    static std::string GetScopingCode();
    static std::string GetProjectEulerProblem1Code();
    static std::string GetAddFunctionCode();
    static std::string GetFibonacciFunctionCode();
    static std::string GetMathAbsCode();
    static std::string GetTonumberCode();
    static std::string GetTostringCode();
    static std::string GetTrigonometricFunctionCode();
    static std::string GetMathFunctionCode();
    static std::string GetStringFunctionCode();
    static std::string GetEratosthenesSieveCode();
};

#endif  // MUA_INTERPRETER_TESTING_CODE_H_