#include <string>

#include "code.h"

std::string Code::GetSampleInputCode() {
    static std::string code = 
            "print(1+2*3/4)\n"
            "--f=math.sin\n"
            "print(math.sin(1.57))\n"
            "--print(f(1.57))\n"
            "print(1<2 and 4+5==9)\n"
            "print(math.max(3,-math.min(5*7,-4)))\n"
            "a={}\n"
            "a[1]={}\n"
            "a[1][2]={}\n"
            "a[1][3]=\"hehe\\\'\\\\\"\n"
            "a[1][1]=a\n"
            "print(#a)\n"
            "print(#a[1])\n"
            "print(#a[1][3])\n"
            "print(a)\n"
            "--print(f)\n"
            "--print(a[1][1][1][3] .. \"\\n\" .. \"..\")\n"
            ;
    return code;
}

std::string Code::GetNewtonMethodCode() {
    static std::string code = 
            "number = 2\n"
            "root = number\n"
            "print(root)\n"
            "root = (number/root + root)/2\n"
            "print(root)\n"
            "root = (number/root + root)/2\n"
            "print(root)\n"
            "root = (number/root + root)/2\n"
            "print(root)\n"
            "root = (number/root + root)/2\n"
            "print(root)\n"
            "root = (number/root + root)/2\n"
            "print(root)\n"
            "root = (number/root + root)/2\n"
            "print(root)\n"
            ;
    return code;
}

std::string Code::GetSuperEasyCode() {
    static std::string code = 
            "print(true and false)\n"
            "print(true or false)\n"
            "print((1+2)*3/4)\n"
            "print(1+2*3/4)\n"
            "print(-1)\n"
            "print(100 % 3)\n"
            "print(\"Hello world, \" .. \"my sweet heart!\")\n"
            "print(\"Hello\"..\" \"..\"--\\\"World\\\"--\")\n"
            "print(\"hehe\\\'\\\\\")\n"
            ;
    return code;
}

std::string Code::GetSimpleStatementCode() {
    static std::string code = 
            "\n"
            ;
    return code;
}

std::string Code::GetWhileStatementCode() {
    static std::string code = 
            "sum = 0\n"
            "x = 0\n"
            "while x <= 100 do\n"
            "    sum = sum + x\n"
            "end\n"
            ;
    return code;
}

std::string Code::GetRepeatStatementCode() {
    static std::string code = 
            "sum = 0\n"
            "x = 0\n"
            "repeat\n"
            "    x = x + 1\n"
            "    sum = sum + x\n"
            "until x >= 100\n"
            ;
    return code;
}

std::string Code::GetLocalVariableDeclarationCode() {
    static std::string code = 
            "local x = 3.14\n"
            "local y\n"
            ;
    return code;
}

std::string Code::GetIfStatementCode() {
    static std::string code = 
            "if d < n then i = -1\n"
            "elseif d > n then i = 1\n"
            "else i = 0\n"
            "end\n"
            ;
    return code;
}

std::string Code::GetBlockStatementCode() {
    static std::string code = 
            "x = 10\n"
            "do\n"
            "  local x = x\n"
            "  print(x)\n"
            "  x = x+1\n"
            "  do\n"
            "    local x = x+1\n"
            "    print(x)\n"
            "  end\n"
            "  print(x)\n"
            "end\n"
            "print(x)\n"
            ;
    return code;
}

std::string Code::GetFunctionDefinitionCode() {
    static std::string code = 
            "function dfs(d)\n"
            "  if d == n then\n"
            "    cnt = cnt + 1\n"
            "  else\n"
            "    for i = 1, n do\n"
            "      if (not vis[i]) and (not vis2[d-i]) and (not vis3[d+i]) then\n"
            "        vis[i] = true\n"
            "        vis2[d-i] = true\n"
            "        vis3[d+i] = true\n"
            "        dfs(d+1)\n"
            "        vis[i] = nil\n"
            "        vis2[d-i] = nil\n"
            "        vis3[d+i] = nil\n"
            "      end\n"
            "    end\n"
            "  end\n"
            "end\n"
            "\n"
            "function swap(x,y)\n"
            "  t = x\n"
            "  x = y\n"
            "  y = t\n"
            "end\n"
            ;
    return code;
}

std::string Code::GetForInStatementCode() {
    static std::string code = 
            "t = {}\n"
            "for key in ipairs(t) do\n"
            "  print(key)\n"
            "end\n"
            ;
    return code;
}

std::string Code::GetSqrt3Code() {
    static std::string code = 
            "number = 3\n"
            "root = number\n"
            "repeat\n"
            "  partial = number / root + root\n"
            "  root = partial/2\n"
            "until root * root - number < 1e-8 and"
            "      number - root * root < 1e-8\n"
            "print(root)\n"
            ;
    return code;
}

std::string Code::GetSumFrom1To10Code() {
    static std::string code = 
            "i = 1\n"
            "sum = 0\n"
            "while i <= 10 do\n"
            "  sum = sum + i\n"
            "  i = i + 1\n"
            "end\n"
            "print(sum)\n"
            ;
    return code;
}

std::string Code::GetBreakableWhileStatementCode() {
    static std::string code = 
            "i = 1\n"
            "sum = 0\n"
            "while i <= 10 do\n"
            "  sum = sum + i\n"
            "  break\n"
            "end\n"
            "print(sum)\n"
            ;
    return code;
}

std::string Code::GetCollatzConjectureCode() {
    static std::string code =
            "i = 100\n"
            "j = 200\n"
            "max_cycle = 1\n"
            "while i <= j do\n"
            "  cycle = 1\n"
            "  n = i\n"
            "  while n ~= 1 do\n"
            "    if n % 2 == 1 then\n" 
            "       n = 3*n + 1\n"
            "    else\n"
            "       n = n/2\n"
            "    end\n"
            "    cycle = cycle + 1\n"
            "  end\n"
            "  --print(cycle)\n"
            "  if cycle > max_cycle then\n"
            "    max_cycle = cycle\n"
            "  end\n"
            "  i = i + 1\n"
            "end\n"
            "print(max_cycle)\n"
            ;
    return code;
}

std::string Code::GetScopingCode() {
    static std::string code =
            "x = 10\n"
            "do\n"
            "  local x = x\n"
            "  print(x)\n"
            "  x = x+1\n"
            "  do\n"
            "    local x = x+1\n"
            "    print(x)\n"
            "  end\n"
            "  print(x)\n"
            "end\n"
            "print(x)\n"
            ;
    return code;
}

std::string Code::GetProjectEulerProblem1Code() {
    static std::string code =
            "sum = 0\n"
            "for n = 1, 999 do\n"
            "  if n % 3 == 0 or n % 5 == 0 then\n"
            "    sum = sum + n\n"
            "  end\n"
            "end\n"
            "print(sum)\n"
            ;
    return code;
}

std::string Code::GetAddFunctionCode() {
    static std::string code =
            "function add(x, y)\n"
            "  print(x + y)\n"
            "  return x + y\n"
            "end\n"
            "\n"
            "print(add(3, 7))\n"
            ;
    return code;
}

std::string Code::GetFibonacciFunctionCode() {
    static std::string code =
            "function fib(n)\n"
            "  rv = 1\n"
            "  if n == 0 then\n"
            "    rv = 0\n"
            "  elseif n == 1 then\n"
            "    rv = 1\n"
            "  else\n" 
            "    rv = fib(n-1) + fib(n-2)\n" 
            "  end\n"
            "  return rv\n"
            "end\n"
            "print(fib(5))\n"
            ;
    return code;
}

std::string Code::GetMathAbsCode() {
    static std::string code =
            "print(math.abs(5))\n"
            "print(math.abs(-5))\n"
            ;
    return code;
}

std::string Code::GetTonumberCode() {
    static std::string code =
            "t = {}\n"
            "print(tonumber(t))\n"
            "print(tonumber(5))\n"
            "print(tonumber(3.1416))\n"
            "print(tonumber(\"-5\"))\n"
            "print(tonumber(\"ABC\"))\n"
            ;
    return code;
}

std::string Code::GetTostringCode() {
    static std::string code =
            "print(tostring(print))\n"
            "t = {}\n"
            "print(tostring(t))\n"
            "print(tostring(5))\n"
            "print(tostring(3.1416))\n"
            "print(tostring(\"-5\"))\n"
            "print(tostring(\"ABC\"))\n"
            ;
    return code;
}

std::string Code::GetTrigonometricFunctionCode() {
    static std::string code =
            "print(\"Sin[0] = \" .. tostring(math.sin(0)))\n"
            "print(math.sin(1))\n"
            "print(math.cos(0))\n"
            "print(math.cos(1))\n"
            "x = math.sin(0.7)\n"
            "y = math.cos(0.7)\n"
            "print(x * x + y * y)\n"
            "print(math.abs(x / y - math.tan(0.7)) < 1e-10)\n"
            "z = math.rad(180) / 3\n"
            "print(math.deg(math.asin(math.sin(z))))\n"
            "print(math.deg(math.acos(math.cos(z))))\n"
            "print(math.deg(math.atan(math.tan(z))))\n"
            "print(math.pi)\n"
            ;
    return code;
}

std::string Code::GetMathFunctionCode() {
    static std::string code =
            "print(\"Abs[-3.4] = \" .. tostring(math.abs(-3.4)))\n"
            "print(\"Floor[-3.4] = \" .. tostring(math.floor(-3.4)))\n"
            "print(\"Ceil[-3.4] = \" .. tostring(math.ceil(-3.4)))\n"
            "print(\"Sqrt[2] = \" .. tostring(math.sqrt(2)))\n"
            "print(\"Exp = \" .. tostring(math.exp(1)))\n"
            "print(\"Log[10] = \" .. tostring(math.log(10)))\n"
            "print(\"Log10[e] = \" .. tostring(math.log10(math.exp(1))))\n"
            "print(\"Min[1, 2] = \" .. tostring(math.min(1, 2)))\n"
            "print(\"Max[1, 2] = \" .. tostring(math.max(1, 2)))\n"
            "print(math.pi)\n"
            ;
    return code;
}

std::string Code::GetStringFunctionCode() {
    static std::string code =
            "print(string.rep(\"Hola\", 3))\n"
            "s = \"PrefixSuffix\"\n"
            "print(string.sub(s, 1, 6))\n"
            "print(string.sub(s, -6))\n"
            ;
    return code;
}

std::string Code::GetEratosthenesSieveCode() {
    static std::string code =
            "visited = {}\n"
            "for i = 1, 3 do\n"
            "  visited[i] = i\n"
            "end\n"
            "for i = 1, 3 do\n"
            "  print(visited[i])\n"
            "end\n"

            "--for i = 2, 10 do\n"
            "--  print(visited[i] == 0)\n"
            "--  if visited[i] == 0 then\n"
            "--  for j = i, 10, i do\n"
            "--    print(j)\n"
            "--  end\n"
            "--  end\n"
            "--end\n"

            ;
    return code;
}