#ifndef MUA_INTERPRETER_TESTING_INTERPRETER_TEST_H_
#define MUA_INTERPRETER_TESTING_INTERPRETER_TEST_H_

#include <string>

class InterpreterTest
{
public:
    static void Run();
    static void Passed();

private:
    static void Interpret(const std::string& code);
};

#endif  // MUA_INTERPRETER_TESTING_INTERPRETER_TEST_H_