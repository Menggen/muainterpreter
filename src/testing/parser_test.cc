#include "code.h"
#include "parser_test.h"

#include "../parser.h"
#include "../source.h"

void ParserTest::Run() {
    ParseWithDebug(Code::GetSqrt3Code());
    Parse(Code::GetIfStatementCode());
    Parse(Code::GetForInStatementCode());
    Parse(Code::GetFunctionDefinitionCode());
    Parse(Code::GetBlockStatementCode());
    Parse(Code::GetLocalVariableDeclarationCode());
    Parse(Code::GetRepeatStatementCode());
    Parse(Code::GetWhileStatementCode());
    Parse(Code::GetSimpleStatementCode());
    Parse(Code::GetSampleInputCode());
}

void ParserTest::Parse(const std::string& code) {
    Source source(code);
    Parser parser(source);
    parser.Parse();
}

void ParserTest::ParseWithDebug(const std::string& code) {
    Source source(code);
    Parser parser(source);
    parser.Parse();
    parser.PrintAbstractSyntaxTree();
}
