#include "code.h"
#include "interpreter_test.h"

#include "../interpreter.h"
#include "../source.h"

void InterpreterTest::Run() {
    Interpret(Code::GetEratosthenesSieveCode());
}

void InterpreterTest::Passed() {
    Interpret(Code::GetStringFunctionCode());
    Interpret(Code::GetMathFunctionCode());
    Interpret(Code::GetTrigonometricFunctionCode());
    Interpret(Code::GetTostringCode());
    Interpret(Code::GetTonumberCode());
    Interpret(Code::GetMathAbsCode());
    Interpret(Code::GetFibonacciFunctionCode());
    Interpret(Code::GetAddFunctionCode());
    Interpret(Code::GetProjectEulerProblem1Code());
    Interpret(Code::GetScopingCode());
    Interpret(Code::GetCollatzConjectureCode());
    Interpret(Code::GetBreakableWhileStatementCode());
    Interpret(Code::GetSumFrom1To10Code());
    Interpret(Code::GetSqrt3Code());
    Interpret(Code::GetSimpleStatementCode());
    Interpret(Code::GetNewtonMethodCode());
    Interpret(Code::GetSuperEasyCode());
}

void InterpreterTest::Interpret(const std::string& code) {
    Source source(code);
    Interpreter interpreter(source);
    interpreter.Interpret();
}
