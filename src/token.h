#ifndef MUA_INTERPRETER_TOKEN_H_
#define MUA_INTERPRETER_TOKEN_H_

#include <string>

enum TokenType
{
    // reserved words
    TK_AND,
    TK_BREAK,
    TK_DO,
    TK_ELSE,
    TK_ELSEIF,
    TK_END,
    TK_FALSE, 
    TK_FOR, 
    TK_FUNCTION, 
    TK_IF,
    TK_IN, 
    TK_LOCAL, 
    TK_NIL, 
    TK_NOT, 
    TK_OR,
    TK_REPEAT, 
    TK_RETURN, 
    TK_THEN, 
    TK_TRUE, 
    TK_UNTIL, 
    TK_WHILE,

    // special symbols
    // +     -     *     /     %     ^     #
    TK_PLUS,
    TK_MINUS,
    TK_STAR,
    TK_SLASH,
    TK_MOD,
    TK_UP_ARROW,
    TK_SHARP,
    // ==    ~=    <=    >=    <     >     =
    TK_EQUALS_EQUALS,
    TK_NOT_EQUALS,
    TK_LESS_EQUALS,
    TK_GREATER_EQUALS,
    TK_LESS,
    TK_GREATER,
    TK_EQUALS,
    // (     )     {     }     [     ]
    TK_LEFT_PAREN,
    TK_RIGHT_PAREN,
    TK_LEFT_BRACE,
    TK_RIGHT_BRACE,
    TK_LEFT_BRACKET,
    TK_RIGHT_BRACKET,
    // ;     :     ,     .     ..    ...
    TK_SEMICOLON,
    TK_COLON,
    TK_COMMA,
    TK_DOT,
    TK_DOT_DOT,
    TK_DOT_DOT_DOT,
    
    TK_IDENTIFIER, 
    TK_NUMBER,
    TK_STRING,
    TK_COMMENT,
    TK_EOL,
    TK_EOS,
    TK_ILLEGAL,

    NUM_TOKENS,
};

class Token 
{
public:
    Token();
    Token(TokenType type);
    Token(TokenType type, std::string text);

    static Token CreateWordToken(const std::string& text);
    static bool IsBinaryOp(const Token& op);
    static bool IsUnaryOp(const Token& op);
    static int GetPrecedence(const Token& op);
    static bool IsRightAssociative(const Token& op);

    TokenType type() const { return type_; }
    std::string text() const { return text_; }
    std::string ToString() const;

private:
    TokenType type_;
    std::string text_;
};

#endif  // MUA_INTERPRETER_TOKEN_H_