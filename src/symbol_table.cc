#include <assert.h>
#include <iostream>
#include <sstream>

#include "ast.h"
#include "symbol_table.h"

Symbol::Symbol(Variable* variable, Node* value)
        : variable_(variable)
        , value_(value)
        , scope_(-1)
{ }

void Symbol::SetScope(int scope) {
    scope_ = scope;
}

// ----------------------------------------------------------------------------

SymbolTable::SymbolTable() {
    symbol_stack_.push_back(SymbolTableHash());
}

void SymbolTable::EnterScope() {
    symbol_stack_.push_back(SymbolTableHash());
}

void SymbolTable::LeaveScope() {
    symbol_stack_.pop_back();
}

void SymbolTable::Insert(Symbol* symbol) {
    assert(symbol);
    assert(!symbol_stack_.empty());
    Symbol* existed = Lookup(symbol->variable());
    symbol->SetScope(existed ? existed->scope() : 0);
    symbol_stack_[symbol->scope()][GetKey(symbol)] = symbol;
}

void SymbolTable::InsertLocal(Symbol* symbol) {
    assert(symbol);
    assert(!symbol_stack_.empty());
    symbol->SetScope(symbol_stack_.size() - 1);
    symbol_stack_[symbol->scope()][GetKey(symbol)] = symbol;
}

Symbol* SymbolTable::Lookup(Variable* variable) {
    std::string key = GetKey(variable);
    std::cout << "<<Symbol Key>> " << key << std::endl;
    for (int i = symbol_stack_.size() - 1; i >= 0; i--) {
        SymbolTableHash curr_table = symbol_stack_[i];
        if (curr_table.find(key) != curr_table.end()) {
            return curr_table[key];
        }
    }
    return 0;
}

int SymbolTable::GetTableSymbolCount(Variable* variable) {
    Symbol* symbol = Lookup(variable);
    assert(symbol);

    std::string expected_name = symbol->variable()->name()->name();
    int expected_length = symbol->variable()->index().size() + 1;
    int count = 0;
    SymbolTableHash table = symbol_stack_[symbol->scope()];
    for (SymbolTableHash::iterator it = table.begin(); 
            it != table.end(); it++)
    {
        Variable* actual_variable = it->second->variable();
        if (actual_variable->name()->name() != expected_name) {
            continue;
        }
        if (actual_variable->index().size() != expected_length) {
            continue;
        }
        count++;
    }
    return count;
}

std::string SymbolTable::GetKey(Symbol* symbol) {
    assert(symbol);
    return GetKey(symbol->variable());
}

std::string SymbolTable::GetKey(Variable* variable) {
    assert(variable);
    return variable->ToString();
}