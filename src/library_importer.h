#ifndef MUA_INTERPRETER_LIBRARY_IMPORTER_H_
#define MUA_INTERPRETER_LIBRARY_IMPORTER_H_

class SymbolTable;

class LibraryImporter
{
public:
    explicit LibraryImporter(SymbolTable* symbol_table);
    void Import();
    
private:
    void ImportLibraryPrintFunction();
    void ImportLibraryTonumberFunction();
    void ImportLibraryTostringFunction();
    void ImportLibraryStringRepFunction();
    void ImportLibraryStringSubFunction();
    void ImportLibraryTableConcatFunction();
    void ImportLibraryTableSortFunction();
    void ImportLibraryMathAbsFunction();
    void ImportLibraryMathFloorFunction();
    void ImportLibraryMathCeilFunction();
    void ImportLibraryMathSqrtFunction();
    void ImportLibraryMathExpFunction();
    void ImportLibraryMathLogFunction();
    void ImportLibraryMathLog10Function();
    void ImportLibraryMathPiConstant();
    void ImportLibraryMathRadFunction();
    void ImportLibraryMathDegFunction();
    void ImportLibraryMathAcosFunction();
    void ImportLibraryMathAsinFunction();
    void ImportLibraryMathAtanFunction();
    void ImportLibraryMathAtan2Function();
    void ImportLibraryMathCosFunction();
    void ImportLibraryMathSinFunction();
    void ImportLibraryMathTanFunction();
    void ImportLibraryMathMinFunction();
    void ImportLibraryMathMaxFunction();

    SymbolTable* symbol_table_;
};

#endif  // MUA_INTERPRETER_LIBRARY_IMPORTER_H_