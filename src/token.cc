#include <assert.h>
#include <map>
#include <set>

#include "token.h"

static const std::string TokenTypeName[NUM_TOKENS] = {
    "TK_AND",
    "TK_BREAK",
    "TK_DO",
    "TK_ELSE",
    "TK_ELSEIF",
    "TK_END",
    "TK_FALSE", 
    "TK_FOR", 
    "TK_FUNCTION", 
    "TK_IF",
    "TK_IN", 
    "TK_LOCAL", 
    "TK_NIL", 
    "TK_NOT", 
    "TK_OR",
    "TK_REPEAT", 
    "TK_RETURN", 
    "TK_THEN", 
    "TK_TRUE", 
    "TK_UNTIL", 
    "TK_WHILE",
    "TK_PLUS",
    "TK_MINUS",
    "TK_STAR",
    "TK_SLASH",
    "TK_MOD",
    "TK_UP_ARROW",
    "TK_SHARP",
    "TK_EQUALS_EQUALS",
    "TK_NOT_EQUALS",
    "TK_LESS_EQUALS",
    "TK_GREATER_EQUALS",
    "TK_LESS",
    "TK_GREATER",
    "TK_EQUALS",
    "TK_LEFT_PAREN",
    "TK_RIGHT_PAREN",
    "TK_LEFT_BRACE",
    "TK_RIGHT_BRACE",
    "TK_LEFT_BRACKET",
    "TK_RIGHT_BRACKET",
    "TK_SEMICOLON",
    "TK_COLON",
    "TK_COMMA",
    "TK_DOT",
    "TK_DOT_DOT",
    "TK_DOT_DOT_DOT",
    "TK_IDENTIFIER", 
    "TK_NUMBER",
    "TK_STRING",
    "TK_COMMENT",
    "TK_EOL",
    "TK_EOS",
    "TK_ILLEGAL",
};

Token::Token()
        : type_(TK_ILLEGAL)
{ }

Token::Token(TokenType type)
        : type_(type)
{ }

Token::Token(TokenType type, std::string text)
        : type_(type)
        , text_(text)
{ }

Token Token::CreateWordToken(const std::string& text) {
    static std::map<std::string, TokenType> ReservedTokens;
    static bool is_first = true;
    if (is_first) {
        ReservedTokens["and"] = TK_AND;
        ReservedTokens["break"] = TK_BREAK, 
        ReservedTokens["do"] = TK_DO,
        ReservedTokens["else"] = TK_ELSE, 
        ReservedTokens["elseif"] = TK_ELSEIF,
        ReservedTokens["end"] = TK_END, 
        ReservedTokens["false"] = TK_FALSE, 
        ReservedTokens["for"] = TK_FOR, 
        ReservedTokens["function"] = TK_FUNCTION, 
        ReservedTokens["if"] = TK_IF,
        ReservedTokens["in"] = TK_IN, 
        ReservedTokens["local"] = TK_LOCAL, 
        ReservedTokens["nil"] = TK_NIL, 
        ReservedTokens["not"] = TK_NOT, 
        ReservedTokens["or"] = TK_OR,
        ReservedTokens["repeat"] = TK_REPEAT, 
        ReservedTokens["return"] = TK_RETURN, 
        ReservedTokens["then"] = TK_THEN, 
        ReservedTokens["true"] = TK_TRUE, 
        ReservedTokens["until"] = TK_UNTIL, 
        ReservedTokens["while"] = TK_WHILE,
        is_first = false;
    }

    if (ReservedTokens.find(text) != ReservedTokens.end()) {
        return Token(ReservedTokens[text], text);
    } 
    else {
        return Token(TK_IDENTIFIER, text);
    }
}

bool Token::IsBinaryOp(const Token& op) {
    static std::set<TokenType> BinaryOps;
    static bool is_first = true;
    if (is_first) {
        BinaryOps.insert(TK_OR);
        BinaryOps.insert(TK_AND);
        BinaryOps.insert(TK_LESS);
        BinaryOps.insert(TK_GREATER);
        BinaryOps.insert(TK_LESS_EQUALS);
        BinaryOps.insert(TK_GREATER_EQUALS);
        BinaryOps.insert(TK_NOT_EQUALS);
        BinaryOps.insert(TK_EQUALS_EQUALS);
        BinaryOps.insert(TK_DOT_DOT);
        BinaryOps.insert(TK_PLUS);
        BinaryOps.insert(TK_MINUS);
        BinaryOps.insert(TK_STAR);
        BinaryOps.insert(TK_SLASH);
        BinaryOps.insert(TK_MOD);
        BinaryOps.insert(TK_UP_ARROW);
        is_first = false;
    }
    return BinaryOps.find(op.type()) != BinaryOps.end();
}

bool Token::IsUnaryOp(const Token& op) {
    TokenType type = op.type();
    return type == TK_NOT || type == TK_SHARP || type == TK_MINUS;
}

int Token::GetPrecedence(const Token& op) {
    assert(IsBinaryOp(op) || IsUnaryOp(op));
    static std::map<TokenType, int> Precedences;
    static bool is_first = true;
    if (is_first) {
        Precedences[TK_OR] = 0;
        Precedences[TK_AND] = 1;
        Precedences[TK_LESS] = 2;
        Precedences[TK_GREATER] = 2;
        Precedences[TK_LESS_EQUALS] = 2;
        Precedences[TK_GREATER_EQUALS] = 2;
        Precedences[TK_NOT_EQUALS] = 2;
        Precedences[TK_EQUALS_EQUALS] = 2;
        Precedences[TK_DOT_DOT] = 3;
        Precedences[TK_PLUS] = 4;
        Precedences[TK_MINUS] = 4;
        Precedences[TK_STAR] = 5;
        Precedences[TK_SLASH] = 5;
        Precedences[TK_MOD] = 5;
        Precedences[TK_UP_ARROW] = 6;
        is_first = false;
    }
    return Precedences[op.type()];
}

bool Token::IsRightAssociative(const Token& op) {
    return op.type() == TK_UP_ARROW;
}

std::string Token::ToString() const {
    return "[" + TokenTypeName[type_] + "] " + text_;
}
