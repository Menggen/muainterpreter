#include <assert.h>
#include <math.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "interpreter.h"

#include "ast.h"
#include "parser.h"
#include "source.h"
#include "symbol_table.h"
#include "runtime_stack.h"
#include "library_importer.h"

Interpreter::Interpreter(Source& source) 
        : source_(source)
        , symbol_table_(new SymbolTable())
        , runtime_stack_(new RuntimeStack())
{ }

Interpreter::~Interpreter() {
    if (symbol_table_) {
        delete symbol_table_;
        symbol_table_ = 0;
    }
    if (runtime_stack_) {
        delete runtime_stack_;
        runtime_stack_ = 0;
    }
}

void Interpreter::Interpret() {
    ImportLibrary();
    Parser parser(source_);
    parser.Parse();
    Execute(parser.ast());
}

void Interpreter::ImportLibrary() {
    LibraryImporter importer(symbol_table_);
    importer.Import();
}

void Interpreter::Execute(Block* block) {
    if (block->AsPrintBlock()) {
        Execute(block->AsPrintBlock());
    }
    else if (block->AsTonumberBlock()) {
        Execute(block->AsTonumberBlock());
    }
    else if (block->AsTostringBlock()) {
        Execute(block->AsTostringBlock());
    }
    else if (block->AsStringRepBlock()) {
        Execute(block->AsStringRepBlock());
    }
    else if (block->AsStringSubBlock()) {
        Execute(block->AsStringSubBlock());
    }
    else if (block->AsTableConcatBlock()) {
        Execute(block->AsTableConcatBlock());
    }
    else if (block->AsTableSortBlock()) {
        Execute(block->AsTableSortBlock());
    }
    else if (block->AsMathAbsBlock()) {
        Execute(block->AsMathAbsBlock());
    }
    else if (block->AsMathFloorBlock()) {
        Execute(block->AsMathFloorBlock());
    }
    else if (block->AsMathCeilBlock()) {
        Execute(block->AsMathCeilBlock());
    }
    else if (block->AsMathSqrtBlock()) {
        Execute(block->AsMathSqrtBlock());
    }
    else if (block->AsMathExpBlock()) {
        Execute(block->AsMathExpBlock());
    }
    else if (block->AsMathLogBlock()) {
        Execute(block->AsMathLogBlock());
    }
    else if (block->AsMathLog10Block()) {
        Execute(block->AsMathLog10Block());
    }
    else if (block->AsMathRadBlock()) {
        Execute(block->AsMathRadBlock());
    }
    else if (block->AsMathDegBlock()) {
        Execute(block->AsMathDegBlock());
    }
    else if (block->AsMathAcosBlock()) {
        Execute(block->AsMathAcosBlock());
    }
    else if (block->AsMathAsinBlock()) {
        Execute(block->AsMathAsinBlock());
    }
    else if (block->AsMathAtanBlock()) {
        Execute(block->AsMathAtanBlock());
    }
    else if (block->AsMathAtan2Block()) {
        Execute(block->AsMathAtan2Block());
    }
    else if (block->AsMathCosBlock()) {
        Execute(block->AsMathCosBlock());
    }
    else if (block->AsMathSinBlock()) {
        Execute(block->AsMathSinBlock());
    }
    else if (block->AsMathTanBlock()) {
        Execute(block->AsMathTanBlock());
    }
    else if (block->AsMathMinBlock()) {
        Execute(block->AsMathMinBlock());
    }
    else if (block->AsMathMaxBlock()) {
        Execute(block->AsMathMaxBlock());
    }
    else {
        ExecuteRegularBlock(block);
    }
}

void Interpreter::ExecuteRegularBlock(Block* block) {
    std::vector<Statement*> statements = block->statements();
    for (std::vector<Statement*>::iterator it = statements.begin();
            it != statements.end(); it++)
    {
        std::cout << "[DEBUG] " << (*it)->ToString() << std::endl;
        Execute(*it);
    }
}

void Interpreter::Execute(Statement* statement) {
    if (statement->AsFunctionDefinition()) {
        Execute(statement->AsFunctionDefinition());
    }
    else if (statement->AsSimpleStatement()) {
        Execute(statement->AsSimpleStatement());
    }
    else if (statement->AsLocalVariableDeclaration()) {
        Execute(statement->AsLocalVariableDeclaration());
    }
    else if (statement->AsWhileStatement()) {
        Execute(statement->AsWhileStatement());
    }
    else if (statement->AsRepeatStatement()) {
        Execute(statement->AsRepeatStatement());
    }
    else if (statement->AsIfStatement()) {
        Execute(statement->AsIfStatement());
    }
    else if (statement->AsForStatement()) {
        Execute(statement->AsForStatement());
    }
    else if (statement->AsReturnStatement()) {
        Execute(statement->AsReturnStatement());
    }
    else if (statement->AsBreakStatement()) {
        Execute(statement->AsBreakStatement());
    }
}

void Interpreter::Execute(SimpleStatement* statement) {
    if (statement->AsEmptyStatement()) {
        Execute(statement->AsEmptyStatement());
    }
    else if (statement->AsFunctionCall()) {
        Execute(statement->AsFunctionCall());
    }
    else if (statement->AsAssignmentStatement()) {
        Execute(statement->AsAssignmentStatement());
    }
    else if (statement->AsBlockStatement()) {
        Execute(statement->AsBlockStatement());
    }
}

void Interpreter::Execute(FunctionDefinition* statement) {
    assert(statement);
    Variable* name = statement->name();
    symbol_table_->Insert(new Symbol(name, statement));
}

void Interpreter::Execute(LocalVariableDeclaration* statement) {
    assert(statement);
    Variable* name = statement->name();
    Expression* value = Evaluate(statement->value());
    symbol_table_->InsertLocal(new Symbol(name, value));
}

void Interpreter::Execute(EmptyStatement* statement) {
    assert(statement);
}

void Interpreter::Execute(AssignmentStatement* statement) {
    assert(statement);
    Variable* name = EvaluateIndex(statement->target());
    Expression* value = Evaluate(statement->value());
    symbol_table_->Insert(new Symbol(name, value));
}

void Interpreter::Execute(FunctionCall* function) {
    assert(function);
    Evaluate(function);
}

void Interpreter::Execute(BlockStatement* statement) {
    symbol_table_->EnterScope();
    Execute(statement->body());
    symbol_table_->LeaveScope();
}

void Interpreter::Execute(WhileStatement* statement) {
    assert(statement);
    while (1) {
        if (IsFalseConditional(statement->condition())) {
            break;
        }
        Execute(statement->body());
        if (runtime_stack_->HasBreakCommand()) {
            runtime_stack_->PopBreakCommand();
            break;
        }
    }
}

void Interpreter::Execute(RepeatStatement* statement) {
    assert(statement);
    while (1) {
        Execute(statement->body());
        if (runtime_stack_->HasBreakCommand()) {
            runtime_stack_->PopBreakCommand();
            break;
        }
        if (!IsFalseConditional(statement->condition())) {
            break;
        }
    }
}

void Interpreter::Execute(IfStatement* statement) {
    assert(statement);
    if (IsFalseConditional(statement->condition())) {
        Execute(statement->else_body());
    } 
    else {
        Execute(statement->then_body());
    }
}

void Interpreter::Execute(ForStatement* statement) {
    assert(statement);
    symbol_table_->EnterScope();

    Variable* each = statement->each();
    Number* initial_number = Evaluate(statement->initial_value())->AsNumber();
    assert(initial_number);
    symbol_table_->InsertLocal(new Symbol(each, initial_number));
    
    Number* limit_number = Evaluate(statement->limit_value())->AsNumber();
    Number* step_number = Evaluate(statement->step_value())->AsNumber();
    assert(limit_number);
    assert(step_number);
    double limit = limit_number->value();
    double step = step_number->value();

    while (1) {
        Number* curr_number = symbol_table_->Lookup(each)->value()->AsNumber();
        assert(curr_number);
        double curr = curr_number->value();
        std::cout << "=>> " << curr << std::endl;
        if ((step > 0 && curr > limit) || (step <= 0 && curr < limit)) {
            break;
        }
        Execute(statement->body());
        if (runtime_stack_->HasBreakCommand()) {
            runtime_stack_->PopBreakCommand();
            break;
        }
        symbol_table_->InsertLocal(new Symbol(each, new Number(curr + step)));
    }
    symbol_table_->LeaveScope();
}

bool Interpreter::IsFalseConditional(Expression* expression) {
    assert(expression);
    SimpleExpression* condition = Evaluate(expression);
    if (condition->AsBoolean() && !condition->AsBoolean()->value()) {
        return true;
    }
    if (condition->AsNil()) {
        return true;
    }
    return false;
}

void Interpreter::Execute(ReturnStatement* statement) {
    assert(statement);
    runtime_stack_->PushReturnValue(Evaluate(statement->value()));
}

void Interpreter::Execute(BreakStatement* statement) {
    assert(statement);
    runtime_stack_->PushBreakCommand();
}

void Interpreter::Execute(PrintBlock* block) {
    assert(block);
    SimpleExpression* e = GetFunctionArgValue("e")->AsSimpleExpression();
    assert(e);
    std::cout << e->ToString() << std::endl;
}

void Interpreter::Execute(TonumberBlock* block) {
    assert(block);
    SimpleExpression* e = GetFunctionArgValue("e")->AsSimpleExpression();
    assert(e);

    if (e->AsNumber()) {
        runtime_stack_->PushReturnValue(e->AsNumber());
    }
    else if (e->AsString()) {
        std::stringstream buffer(e->AsString()->value());
        double rv; 
        if ((buffer >> rv).fail()) {
            runtime_stack_->PushReturnValue(new Nil());
        }
        else {
            runtime_stack_->PushReturnValue(new Number(rv));
        }
    }
    else {
        runtime_stack_->PushReturnValue(new Nil());
    }
}

void Interpreter::Execute(TostringBlock* block) {
    assert(block);
    SimpleExpression* e = GetFunctionArgValue("e")->AsSimpleExpression();
    assert(e);

    if (e->AsEmptyTable()) {
        runtime_stack_->PushReturnValue(new String("table"));
    }
    else if (e->AsVariable()) {
        Symbol* function = symbol_table_->Lookup(e->AsVariable());
        if (function->value()->AsFunctionDefinition()) {
            runtime_stack_->PushReturnValue(new String("function"));        
        }
    }
    else {
        runtime_stack_->PushReturnValue(new String(e->ToString()));
    }
}

void Interpreter::Execute(StringRepBlock* block) {
    assert(block);
    String* s = GetFunctionArgValue("s")->AsString();
    Number* n = GetFunctionArgValue("n")->AsNumber();
    assert(s);
    assert(n);

    std::stringstream rv;
    for (int i = 0; i < n->value(); i++) {
        rv << s->value();
    }
    runtime_stack_->PushReturnValue(new String(rv.str()));
}

void Interpreter::Execute(StringSubBlock* block) {
    assert(block);
    String* s = GetFunctionArgValue("s")->AsString();
    Number* i = GetFunctionArgValue("i")->AsNumber();
    Number* j = GetFunctionArgValue("j")->AsNumber();
    assert(s);
    assert(i);
    int begin = i->value();
    int end = (j) ? j->value() : -1;
    int length = s->value().size();
    if (begin < 0) {
        begin = begin + length + 1;
    }
    if (end < 0) {
        end = end + length + 1;
    }
    std::string rv = s->value().substr(begin - 1, end - begin + 1); 
    runtime_stack_->PushReturnValue(new String(rv));
}

void Interpreter::Execute(TableConcatBlock* block) {
    assert(block);
}

void Interpreter::Execute(TableSortBlock* block) {
    assert(block);
}

void Interpreter::Execute(MathAbsBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    runtime_stack_->PushReturnValue(new Number(abs(x->value())));
}

void Interpreter::Execute(MathFloorBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    runtime_stack_->PushReturnValue(new Number(floor(x->value())));
}

void Interpreter::Execute(MathCeilBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    runtime_stack_->PushReturnValue(new Number(ceil(x->value())));
}

void Interpreter::Execute(MathSqrtBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    runtime_stack_->PushReturnValue(new Number(sqrt(x->value())));
}

void Interpreter::Execute(MathExpBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    runtime_stack_->PushReturnValue(new Number(exp(x->value())));
}

void Interpreter::Execute(MathLogBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    runtime_stack_->PushReturnValue(new Number(log(x->value())));
}

void Interpreter::Execute(MathLog10Block* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    runtime_stack_->PushReturnValue(new Number(log10(x->value())));
}

void Interpreter::Execute(MathRadBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    double PI = acos(-1.0);
    double rv = x->value() * PI / 180.0;
    runtime_stack_->PushReturnValue(new Number(rv));
}

void Interpreter::Execute(MathDegBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    double PI = acos(-1.0);
    double rv = x->value() * 180.0 / PI;
    runtime_stack_->PushReturnValue(new Number(rv));
}

void Interpreter::Execute(MathAcosBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    runtime_stack_->PushReturnValue(new Number(acos(x->value())));
}

void Interpreter::Execute(MathAsinBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    runtime_stack_->PushReturnValue(new Number(asin(x->value())));
}

void Interpreter::Execute(MathAtanBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    runtime_stack_->PushReturnValue(new Number(atan(x->value())));
}

void Interpreter::Execute(MathAtan2Block* block) {
    assert(block);
    Number* y = GetFunctionArgValue("y")->AsNumber();
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(y);
    assert(x);
    runtime_stack_->PushReturnValue(new Number(atan2(y->value(), x->value())));
}

void Interpreter::Execute(MathCosBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    runtime_stack_->PushReturnValue(new Number(cos(x->value())));
}

void Interpreter::Execute(MathSinBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    runtime_stack_->PushReturnValue(new Number(sin(x->value())));
}

void Interpreter::Execute(MathTanBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    assert(x);
    runtime_stack_->PushReturnValue(new Number(tan(x->value())));
}

void Interpreter::Execute(MathMinBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    Number* y = GetFunctionArgValue("y")->AsNumber();
    assert(x);
    assert(y);
    double rv = (x->value() < y->value()) ? x->value() : y->value();
    runtime_stack_->PushReturnValue(new Number(rv));
}

void Interpreter::Execute(MathMaxBlock* block) {
    assert(block);
    Number* x = GetFunctionArgValue("x")->AsNumber();
    Number* y = GetFunctionArgValue("y")->AsNumber();
    assert(x);
    assert(y);
    double rv = (x->value() > y->value()) ? x->value() : y->value();
    runtime_stack_->PushReturnValue(new Number(rv));
}

Node* Interpreter::GetFunctionArgValue(const std::string& name) {
    Variable* arg = new Variable(
            new Identifier(name), std::vector<Expression*>());
    Symbol* symbol = symbol_table_->Lookup(arg);
    if (symbol) {
        return symbol->value(); 
    }
    else {
        return new Nil();
    }
}

// ----------------------------------------------------------------------------

SimpleExpression* Interpreter::Evaluate(Expression* expression) {
    if (expression->AsSimpleExpression()) {
        return Evaluate(expression->AsSimpleExpression());
    }
    if (expression->AsBinaryExpression()) {
        return Evaluate(expression->AsBinaryExpression());
    }
    if (expression->AsUnaryExpression()) {
        return Evaluate(expression->AsUnaryExpression());
    }
    if (expression->AsParenExpression()) {
        return Evaluate(expression->AsParenExpression());
    }
    assert(false);
    return new Nil();
}

SimpleExpression* Interpreter::Evaluate(SimpleExpression* expression) {
    if (expression->AsNil() || expression->AsBoolean() || 
            expression->AsNumber() || expression->AsString() || 
            expression->AsEmptyTable())
    {
        return expression;
    }
    if (expression->AsVariable()) {
        return Evaluate(expression->AsVariable());
    }
    if (expression->AsFunctionCall()) {
        return Evaluate(expression->AsFunctionCall());
    }
    assert(false);
    return new Nil();
}

SimpleExpression* Interpreter::Evaluate(BinaryExpression* expression) {
    assert(expression);
    SimpleExpression* left = Evaluate(expression->left());
    SimpleExpression* right = Evaluate(expression->right());
    TokenType type = expression->op().type();
    if (type == TK_AND) {
        return And(left, right);
    }
    if (type == TK_OR) {
        return Or(left, right);
    }
    if (type == TK_LESS) {
        return Less(left, right);
    }
    if (type == TK_GREATER) {
        return Greater(left, right);
    }
    if (type == TK_LESS_EQUALS) {
        return LessEqual(left, right);
    }
    if (type == TK_GREATER_EQUALS) {
        return GreaterEqual(left, right);
    }
    if (type == TK_NOT_EQUALS) {
        return NotEqual(left, right);
    }
    if (type == TK_EQUALS_EQUALS) {
        return Equal(left, right);
    }
    if (type == TK_DOT_DOT) {
        return Concatenate(left, right);
    }
    if (type == TK_PLUS) {
        return Plus(left, right);
    }
    if (type == TK_MINUS) {
        return Minus(left, right);
    }
    if (type == TK_STAR) {
        return Multiple(left, right);
    }
    if (type == TK_SLASH) {
        return Divide(left, right);
    }
    if (type == TK_MOD) {
        return Mod(left, right);
    }
    if (type == TK_UP_ARROW) {
        return Pow(left, right);
    }
    return new Nil();
}

SimpleExpression* Interpreter::Evaluate(UnaryExpression* expression) {
    assert(expression);
    SimpleExpression* operand = Evaluate(expression->operand());
    TokenType type = expression->op().type();
    assert(type == TK_NOT || type == TK_MINUS || type == TK_SHARP);
    if (type == TK_NOT) {
        return Not(operand);
    }
    if (type == TK_MINUS) {
        return Minus(operand);
    }
    if (type == TK_SHARP) {
        if (operand->AsString()) {
            return StringLength(operand);
        }
        else {
            return TableLength(expression->operand()->AsVariable());
        }
    }
    return new Nil();
}

SimpleExpression* Interpreter::Evaluate(ParenExpression* expression) {
    assert(expression);
    return Evaluate(expression->value());
}

SimpleExpression* Interpreter::Evaluate(Variable* expression) {
    assert(expression);
    expression = EvaluateIndex(expression);
    Symbol* symbol = symbol_table_->Lookup(expression);
    if (symbol->value()->AsExpression()) {
        return Evaluate(symbol->value()->AsExpression());
    }
    else {
        return expression;
    }
}

Variable* Interpreter::EvaluateIndex(Variable* expression) {
    assert(expression);
    std::vector<Expression*> index = expression->index();
    for (int i = 0; i < index.size(); i++) {
        index[i] = Evaluate(index[i]);
    }
    expression->SetIndex(index);
    return expression;
}

SimpleExpression* Interpreter::Evaluate(FunctionCall* expression) {
    assert(expression);
    Symbol* symbol = symbol_table_->Lookup(expression->callee());
    assert(symbol);
    FunctionDefinition* def = symbol->value()->AsFunctionDefinition();
    symbol_table_->EnterScope();
    FillFunctionCallArgs(expression, def);
    Execute(def->body());
    symbol_table_->LeaveScope();
    return runtime_stack_->HasReturnValue() ? 
            runtime_stack_->PopReturnValue() : new Nil();
}

void Interpreter::FillFunctionCallArgs(FunctionCall* call, 
                                       FunctionDefinition* definition)
{
    int actual_argc = call->args().size();
    int expected_argc = definition->parameters().size();
    assert(actual_argc <= expected_argc);
    for (int i = 0; i < actual_argc; i++) {
        Variable* name = (definition->parameters())[i];
        Expression* value = Evaluate((call->args())[i]);
        symbol_table_->InsertLocal(new Symbol(name, value));
    }
}

Boolean* Interpreter::Not(SimpleExpression* operand) {
    assert(operand->AsBoolean());
    bool rv = !operand->AsBoolean()->value();
    return new Boolean(rv);
}

Number* Interpreter::Minus(SimpleExpression* operand) {
    assert(operand->AsNumber());
    double rv = - operand->AsNumber()->value();
    return new Number(rv);
}

Number* Interpreter::StringLength(SimpleExpression* operand) {
    assert(operand->AsString());
    return new Number(operand->AsString()->value().size());
}

Number* Interpreter::TableLength(Variable* operand) {
    assert(operand);
    return new Number(symbol_table_->GetTableSymbolCount(operand));
}

Boolean* Interpreter::And(SimpleExpression* left, SimpleExpression* right) {
    assert(left->AsBoolean());
    assert(right->AsBoolean());
    bool rv = left->AsBoolean()->value() && right->AsBoolean()->value();
    return new Boolean(rv);
}

Boolean* Interpreter::Or(SimpleExpression* left, SimpleExpression* right) {
    assert(left->AsBoolean());
    assert(right->AsBoolean());
    bool rv = left->AsBoolean()->value() || right->AsBoolean()->value();
    return new Boolean(rv);
}

Boolean* Interpreter::Less(SimpleExpression* left, SimpleExpression* right) {
    assert(left->AsNumber());
    assert(right->AsNumber());
    bool rv = left->AsNumber()->value() < right->AsNumber()->value();
    return new Boolean(rv);
}

Boolean* Interpreter::Greater(SimpleExpression* left, 
                              SimpleExpression* right) 
{
    assert(left->AsNumber());
    assert(right->AsNumber());
    bool rv = left->AsNumber()->value() > right->AsNumber()->value();
    return new Boolean(rv);
}

Boolean* Interpreter::LessEqual(SimpleExpression* left, 
                                SimpleExpression* right) 
{
    assert(left->AsNumber());
    assert(right->AsNumber());
    bool rv = left->AsNumber()->value() <= right->AsNumber()->value();
    return new Boolean(rv);
}

Boolean* Interpreter::GreaterEqual(SimpleExpression* left, 
                                   SimpleExpression* right) 
{
    assert(left->AsNumber());
    assert(right->AsNumber());
    bool rv = left->AsNumber()->value() >= right->AsNumber()->value();
    return new Boolean(rv);
}

Boolean* Interpreter::NotEqual(SimpleExpression* left, 
                               SimpleExpression* right) 
{
    assert(left->AsNumber());
    assert(right->AsNumber());
    bool rv = left->AsNumber()->value() != right->AsNumber()->value();
    return new Boolean(rv);
}

Boolean* Interpreter::Equal(SimpleExpression* left, 
                            SimpleExpression* right) 
{
    bool rv = false;
    if (left->AsNumber() && right->AsNumber()) {
        rv = left->AsNumber()->value() == right->AsNumber()->value();
    }
    else if (left->AsBoolean() && right->AsBoolean()) {
        rv = left->AsBoolean()->value() == right->AsBoolean()->value();
    }
    else {
        assert(false);
    }
    return new Boolean(rv);
}

String* Interpreter::Concatenate(SimpleExpression* left, 
                                 SimpleExpression* right)
{
    assert(left->AsString());
    assert(right->AsString());
    std::string rv = left->AsString()->value() + right->AsString()->value();
    return new String(rv);
}

Number* Interpreter::Plus(SimpleExpression* left, SimpleExpression* right) {
    assert(left->AsNumber());
    assert(right->AsNumber());
    double rv = left->AsNumber()->value() + right->AsNumber()->value();
    return new Number(rv);
}

Number* Interpreter::Minus(SimpleExpression* left, SimpleExpression* right) {
    assert(left->AsNumber());
    assert(right->AsNumber());
    double rv = left->AsNumber()->value() - right->AsNumber()->value();
    return new Number(rv);
}

Number* Interpreter::Multiple(SimpleExpression* left, 
                              SimpleExpression* right) 
{
    assert(left->AsNumber());
    assert(right->AsNumber());
    double rv = left->AsNumber()->value() * right->AsNumber()->value();
    return new Number(rv);
}

Number* Interpreter::Divide(SimpleExpression* left, SimpleExpression* right) {
    assert(left->AsNumber());
    assert(right->AsNumber());
    double rv = left->AsNumber()->value() / right->AsNumber()->value();
    return new Number(rv);
}

Number* Interpreter::Mod(SimpleExpression* left, SimpleExpression* right) {
    assert(left->AsNumber());
    assert(right->AsNumber());
    double base = left->AsNumber()->value();
    double mod = right->AsNumber()->value();
    double rv = base - floor(base/mod) * mod;
    return new Number(rv);
}

Number* Interpreter::Pow(SimpleExpression* left, SimpleExpression* right) {
    assert(left->AsNumber());
    assert(right->AsNumber());
    double base = left->AsNumber()->value();
    double power = right->AsNumber()->value();
    double rv = pow(base, power);
    return new Number(rv);
}
